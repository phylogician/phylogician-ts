<img src="https://gitlab.com/phylogician/phylogician-ts/raw/master/assets/PhylogicianTS.banner.png" width=500px/>

![npm](https://img.shields.io/npm/v/phylogician-ts.svg)
![License: CC0-1.0](https://img.shields.io/badge/License-CC0%201.0-blue.svg)
[![pipeline status](https://gitlab.com/phylogician/phylogician-ts/badges/master/pipeline.svg)](https://gitlab.com/phylogician/phylogician-ts/commits/master)
[![coverage report](https://gitlab.com/phylogician/phylogician-ts/badges/master/coverage.svg)](https://phylogician.gitlab.io/phylogician-ts/coverage)

# PhylogicianTS

Module to read, manipulate and write phylogenetic trees.

## Install

```shell
npm install phylogician-ts
```

## Usage

```typescript
import { Tree } from 'phylogician-ts'

const newick = '(A:2,(B:3,C:4):5);'
const tree = new Tree()

tree.buildTree(nwk)

console.log(tree)

/*
Tree {
  nodes:
    [
      TreeNode {
        name: '',
        branchLength: null,
        children: [Array],
        id: 0,
        parent: null,
        root: true
      },
     TreeNode {
        name: 'A',
        branchLength: 2,
        children: [],
        id: 1,
        parent: 0,
        root: false
      },
     ...
*/
```

## Documentation

[Developer's Documentation](https://phylogician.gitlab.io/phylogician-ts/)

... to be continued.

Written with ❤ in Typescript.
