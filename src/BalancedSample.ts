import { Logger, LogLevelDescType } from 'loglevel-colored-prefix';
import seedrandom from 'seedrandom';
import { Tree } from './Tree';

interface ISampleTracker {
  id: number;
  assignedSamples: number;
}

class BalancedSample {
  private tree: Tree;
  private nodePickCounter: number[];
  private logger: Logger;
  private logLevel: LogLevelDescType;
  private sampleTracker: ISampleTracker[];

  constructor(tree: Tree, loglevel: LogLevelDescType = 'info') {
    this.tree = tree;
    this.nodePickCounter = Array(this.tree.nodes.length).fill(1);
    this.logLevel = loglevel;
    this.logger = new Logger(this.logLevel);
    this.sampleTracker = [];
  }

  public getScores(): number[] {
    return this.nodePickCounter;
  }

  public setScores(): this {
    const log = this.logger.getLogger('BalancedSample::setScores');
    log.info(`setting the node scores`);
    const setScoresFromList = (nodeId: number) => {
      let numberOfLeafs = 0;
      this.tree.nodes[nodeId].children.forEach(childId => {
        const childNode = this.tree.nodes[childId];
        numberOfLeafs += childNode.isLeaf() ? 1 : setScoresFromList(childId);
      });
      this.nodePickCounter[nodeId] = numberOfLeafs;
      log.debug(`node ${nodeId} has ${numberOfLeafs} leafs.`);
      return numberOfLeafs;
    };

    const rootNode = this.tree.getRootNode();
    if (rootNode) {
      this.nodePickCounter[rootNode.id] = setScoresFromList(rootNode.id);
    }
    return this;
  }

  public pick(picked: number[], baseNodeId: number, N: number, add: boolean = false, seed?: string): number[] {
    return this.setScores().selectBalancedSample(picked, baseNodeId, N, add, seed);
  }

  public pickMixed(picked: number[], baseNodeId: number, N: number, add: boolean = false, seed?: string): number[] {
    return this.setScores().selectBalancedSample2(picked, baseNodeId, N, add, seed);
  }

  public pickRandom(N: number, listOfNodes: number[], seed?: string | undefined) {
    const log = this.logger.getLogger('BalancedSample::pickRandom');
    const rng = seedrandom(seed);
    const sample = [];
    if (listOfNodes.length < N) {
      return listOfNodes;
    }
    log.debug(`picking ${N} samples from ${listOfNodes}`);
    while (sample.length < N) {
      const rn = rng();
      const i = rn * (listOfNodes.length - 1);
      log.debug(`current List of nodes: ${listOfNodes}`);
      log.debug(`value: ${i}`);
      sample.push(listOfNodes.splice(i, 1)[0]);
      log.debug(`tmp sample: ${sample}\n`);
    }
    return sample;
  }

  private selectBalancedSample2(
    picked: number[] = [],
    baseNodeIds: number,
    N: number,
    add: boolean = false,
    seed?: string,
    thread: number[] = [],
  ): number[] {
    const log = this.logger.getLogger('BalancedSample::selectBalancedSample2');
    log.debug(`Working on ${baseNodeIds} to pick ${N} samples.`);
    let samples: number[] = [];
    const rng = seedrandom(seed);
    const children = this.tree.nodes[baseNodeIds].children;
    const leafs = children.filter(id => this.tree.nodes[id].isLeaf());
    const internal = children.filter(id => !this.tree.nodes[id].isLeaf());
    const odds = [leafs.length];
    const maxValue = internal.reduce((sum, b, i) => (odds[i + 1] = sum + this.nodePickCounter[b]), leafs.length);

    const sampleAssignment = Array(odds.length).fill(0);

    log.debug(`options: ${leafs.length}-leafs,${internal}`);
    log.debug(`maxLength: ${maxValue} odds ${JSON.stringify(odds)}`);
    for (let i = 0; i < N; i++) {
      const rnum = rng();
      const value = rnum * maxValue;
      let choice = 0;
      while (choice < odds.length) {
        if (odds[choice] > value) {
          break;
        }
        choice++;
      }
      sampleAssignment[choice]++;
    }
    log.debug(`sampleAssignment: ${sampleAssignment}`);
    const rn = rng();
    samples = samples.concat(this.pickRandom(sampleAssignment[0], leafs, rn.toString()));

    if (sampleAssignment[0] > samples.length) {
      sampleAssignment[1] += sampleAssignment[0] - samples.length;
    }

    internal.forEach((id, i) => {
      const n = sampleAssignment[i + 1];
      log.debug(`follow up on ${id} to pick ${n}`);
      if (n > 0) {
        if (this.nodePickCounter[id] <= n) {
          samples = samples.concat(this.tree.getLeafsIds(id));
          if (i < internal.length) {
            sampleAssignment[i + 2] += n - this.nodePickCounter[id];
          }
        } else if (n === 1) {
          const newSeed = rng();
          samples = samples.concat(this.selectBalancedSample([], id, 1, false, newSeed.toString()));
        } else {
          const newSeed = rng();
          samples = samples.concat(this.selectBalancedSample2([], id, n, false, newSeed.toString()));
        }
      } else {
        log.debug(`skip`);
      }
    });

    return samples;
  }

  private selectBalancedSample(
    picked: number[] = [],
    baseNodeId: number,
    N: number,
    add: boolean = false,
    seed?: string,
    thread: number[] = [],
  ): number[] {
    const log = this.logger.getLogger('BalancedSample::selectBalancedSample');
    const selected: number[] = picked.map(id => id);
    const rng = seedrandom(seed);
    const baseNode = this.tree.nodes[baseNodeId];
    let paths = baseNode.children.filter(d => selected.indexOf(d) === -1 && this.nodePickCounter[d] > 0);
    log.debug(`Init search for ${baseNodeId}`);
    log.debug(`already selected: ${selected}`);
    log.debug(`possible paths: ${paths}`);
    const totalNumber = add ? picked.length + N : N;
    log.debug(`will pick additional: ${totalNumber - selected.length}`);
    while (selected.length < totalNumber && paths.length > 0) {
      const thisThread = thread.map(id => id);
      thisThread.push(baseNodeId);
      log.debug(`${thisThread} - getting to tree ${baseNodeId}`);
      log.debug(`${thisThread} - state of counter: ${JSON.stringify(this.nodePickCounter)}`);
      const rn = rng();
      paths = baseNode.children.filter(d => selected.indexOf(d) === -1 && this.nodePickCounter[d] > 0);
      log.debug(`${thisThread} - selected: ${selected}`);
      log.debug(`${thisThread} - possible paths ${paths}`);
      const ruler: number[] = [];
      const fullLength = paths.reduce((a, b, i) => (ruler[i] = a + this.nodePickCounter[b]), 0);
      log.debug(`${thisThread} - maxLength: ${fullLength} ruler ${JSON.stringify(ruler)}`);
      const value = rn * fullLength;
      log.debug(`${thisThread} - value: ${value}`);
      let choice = 0;
      while (choice < paths.length) {
        if (ruler[choice] > value) {
          break;
        }
        choice++;
      }
      const nextNode = this.tree.nodes[paths[choice]];
      log.debug(`${thisThread} - pick: ${nextNode.id}\n`);
      if (!nextNode.isLeaf()) {
        log.debug(`${thisThread} - ${nextNode.id} is not a leaf\n`);
        const newSelected = this.selectBalancedSample(selected, nextNode.id, 1, true, rn.toString(), thisThread);
        log.debug(
          `${thisThread} - old selection: ${selected} new selection: ${newSelected} from ${nextNode.id} for ${baseNodeId}\n`,
        );
        if (newSelected.toString() === selected.toString()) {
          log.debug(`${thisThread} - eliminating path ${paths[choice]}: no use`);
          paths.splice(choice, 1);
        } else {
          log.debug(`${thisThread} - Found ${newSelected[newSelected.length - 1]} on path ${paths[choice]}: adding`);
          selected.push(newSelected[newSelected.length - 1]);
        }
      } else {
        log.debug(`${thisThread} - is leaf!\n`);
        selected.push(nextNode.id);
        thisThread.forEach(id => {
          this.nodePickCounter[id]--;
        });
      }
    }
    return selected;
  }
}

export { BalancedSample };
