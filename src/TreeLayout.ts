import * as d3 from 'd3';
import { IDisplayNameChunks, ISvgParams } from './interfaces';
import { Tree } from './Tree';
import { TreeNode } from './TreeNode';

abstract class TreeLayout {
  public tree: Tree;
  public svgParams: ISvgParams;

  constructor(tree: Tree, svgParams: ISvgParams) {
    this.tree = tree;
    this.svgParams = svgParams;
  }

  public abstract calcTree(scaling: boolean): void;
  public abstract calcBranchPath(): TreeLayout;
  public abstract labelPlace(id: number): string;
  public abstract labelAnchor(id: number): string;

  public calcLabelDims(): TreeLayout {
    this.tree.nodes.forEach(node => {
      const svg = d3.select('body').append('svg');
      const text = this.renderNames(node, svg, true);
      // .attr('display', node.getShowLabel() ? 'block' : 'none');

      const domEl = text.node();
      if (!domEl) {
        throw new Error('Cannot find any DOM element with this selection.');
      }
      const dim = domEl.getBBox();
      const h: number = dim.height;
      const w: number = dim.width;
      node.setLabelHeight(h);
      node.setLabelWidth(w);
      svg.remove();
    });
    return this;
  }

  /**
   * Helper function to render names
   *
   * @param {TreeNode} node
   * @param {d3.Selection<any, any, any, any>} svg
   * @param {boolean} [tmp=false]
   * @returns
   * @memberof TreeLayout
   */
  public renderNames(node: TreeNode, svg: d3.Selection<any, any, any, any>, tmp: boolean = false) {
    const label = svg.append('text').attr('class', tmp ? 'labeltmp' : 'label');
    this.updateNames(node, label);
    return label;
  }

  /**
   * Helper function to update names
   *
   * @param {TreeNode} node
   * @param {d3.Selection<any, any, any, any>} svg
   * @param {boolean} [tmp=false]
   * @returns
   * @memberof TreeLayout
   */
  public updateNames(node: TreeNode, label: d3.Selection<any, any, any, any>, duration: number = 0) {
    const chunks = node.getDisplayNameChunks();
    if (chunks.length > 0) {
      label
        .selectAll('tspan')
        .data(chunks) // , function (d: IDisplayNameChunks) { return this.id })
        .join(
          enter =>
            enter
              .append('tspan')
              .text((l: IDisplayNameChunks) => l.value)
              .attr('font-size', (l: IDisplayNameChunks) => l.font.size)
              .attr('font-style', (l: IDisplayNameChunks) => l.font.style)
              .attr('fill', (l: IDisplayNameChunks) => l.color),
          update =>
            update.call(u =>
              u
                .transition()
                .duration(duration)
                .text((l: IDisplayNameChunks) => l.value)
                .attr('font-size', (l: IDisplayNameChunks) => l.font.size)
                .attr('font-style', (l: IDisplayNameChunks) => l.font.style)
                .attr('fill', (l: IDisplayNameChunks) => l.color),
            ),
          exit => exit.call(e => e.remove()),
        );
    }
  }

  /**
   * It gets the longest label. It skips labels marked to not show.
   *
   * @returns {number}
   * @memberof TreeLayout
   */
  protected getLongestLabel(): number {
    let longestLabel = 0;
    this.tree.nodes.forEach(node => {
      if (node.getShowLabel()) {
        const labelWidth = node.getLabelWidth();
        longestLabel = longestLabel < labelWidth ? labelWidth : longestLabel;
      }
    });
    return longestLabel;
  }
}

export { TreeLayout };
