import { IDisplayNameChunks, IDisplayNameUserChunks, IOptionsViz } from './interfaces';

export class TreeNode {
  public name: string;
  public branchLength: number | null;
  public children: number[];
  public parent: number | null;
  public reverseLadderized: boolean;
  public readonly id: number;
  protected displayNameChunks: IDisplayNameChunks[];
  protected displayNameUserChunks: IDisplayNameUserChunks[];
  protected viz: IOptionsViz;
  protected root: boolean;

  /**
   * Creates an instance of TreeNode.
   * @param {string} name
   * @param {(number|null)} branchLength
   * @memberof TreeNode
   */
  constructor(name: string, branchLength: number | null, id: number) {
    this.name = name;
    this.branchLength = branchLength;
    this.children = [];
    this.id = id;
    this.parent = null;
    this.reverseLadderized = false;
    this.root = false;
    this.viz = {
      branch: {
        color: 'black',
        opacity: 1,
        path: '',
        width: 1,
      },
      label: {
        color: 'black',
        dim: {
          h: 0,
          w: 0,
        },
        font: {
          family: '',
          size: '16px',
          style: 'normal',
        },
        show: true,
      },
      node: {
        color: 'black',
        shape: 'circle',
        size: 3,
      },
      pos: {
        a: 0,
        r: 0,
        x: 0,
        y: 0,
      },
    };
    this.displayNameChunks = [
      {
        ...this.viz.label,
        value: this.name,
      },
    ];
    this.displayNameUserChunks = [
      {
        value: this.name,
      },
    ];
  }

  /**
   * Sets parent to the Tree node.
   *
   * @param {Tree} parent
   * @returns {TreeNode}
   * @memberof Tree
   */
  public setParentNodeId(parent: number): this {
    this.parent = parent;
    return this;
  }

  /**
   * Returns parent of the node
   *
   * @returns {(number | null)}
   * @memberof TreeNode
   */
  public getParentNodeId(): number | null {
    return this.parent;
  }

  /**
   * Returns true if the node is leaf
   *
   * @returns {boolean}
   * @memberof TreeNode
   */
  public isLeaf(): boolean {
    return this.children.length === 0 ? true : false;
  }

  /**
   * Sets node to be root
   *
   * @memberof TreeNode
   */
  public setAsRoot(): this {
    this.root = true;
    return this;
  }

  /**
   * Sets node to not be root
   *
   * @memberof TreeNode
   */
  public unsetAsRoot(): this {
    this.root = false;
    return this;
  }

  /**
   *
   *
   * @returns {boolean}
   * @memberof TreeNode
   */
  public isRoot(): boolean {
    return this.root;
  }

  /**
   * Returns the size of the node
   *
   * @returns {number}
   * @memberof TreeNode
   */
  public getNodeSize(): number {
    return this.viz.node.size;
  }

  /**
   * Changes the node size
   *
   * @param {number} size
   * @memberof TreeNode
   */
  public setNodeSize(size: number): this {
    this.viz.node.size = size;
    return this;
  }

  /**
   * Returns the color of the node
   *
   * @returns {string} node color
   * @memberof TreeNode
   */
  public getNodeColor(): string {
    return this.viz.node.color;
  }

  /**
   * Changes the node color.
   *
   * @param {string} color
   * @memberof TreeNode
   */
  public setNodeColor(color: string): this {
    this.viz.node.color = color;
    return this;
  }

  /**
   * Returns the shape of the node
   *
   * @returns {string} node shape
   * @memberof TreeNode
   */
  public getNodeShape(): string {
    return this.viz.node.shape;
  }

  /**
   * Changes the node shape.
   *
   * @param {string} shape
   * @memberof TreeNode
   */
  public setNodeShape(shape: string): this {
    this.viz.node.shape = shape;
    return this;
  }

  /**
   * Returns the position X of the node
   *
   * @returns {number} position X
   * @memberof TreeNode
   */
  public getPosX(): number {
    return this.viz.pos.x;
  }

  /**
   * Changes the position X of the node.
   *
   * @param {number} x
   * @memberof TreeNode
   */
  public setPosX(x: number): this {
    this.viz.pos.x = x;
    return this;
  }

  /**
   * Returns the position Y of the node
   *
   * @returns {number} position Y
   * @memberof TreeNode
   */
  public getPosY(): number {
    return this.viz.pos.y;
  }

  /**
   * Changes the position Y of the node.
   *
   * @param {number} y
   * @memberof TreeNode
   */
  public setPosY(y: number): this {
    this.viz.pos.y = y;
    return this;
  }

  /**
   * Returns the position "r" of the node (for Circular layout)
   *
   * @returns {number} position R
   * @memberof TreeNode
   */
  public getPosR(): number {
    return this.viz.pos.r;
  }

  /**
   * Changes the position "r" of the node (for Circular layout).
   *
   * @param {number} r
   * @memberof TreeNode
   */
  public setPosR(r: number): this {
    this.viz.pos.r = r;
    return this;
  }

  /**
   * Returns the position "a" of the node (for Circular layout)
   *
   * @returns {number} position Y
   * @memberof TreeNode
   */
  public getPosA(): number {
    return this.viz.pos.a;
  }

  /**
   * Changes the position "a" of the node (for Circular layout).
   *
   * @param {number} a
   * @memberof TreeNode
   */
  public setPosA(a: number): this {
    this.viz.pos.a = a;
    return this;
  }

  /**
   * Sets the branch path of the node.
   *
   * @param {string} path
   * @memberof TreeNode
   */
  public setBranchPath(path: string | null): this {
    this.viz.branch.path = path || '';
    return this;
  }

  /**
   * Gets the branch path of the node.
   *
   * @returns {string}
   * @memberof TreeNode
   */
  public getBranchPath(): string {
    return this.viz.branch.path;
  }

  /**
   * Sets the branch path of the node.
   *
   * @param {string} path
   * @memberof TreeNode
   */
  public setBranchColor(color: string): this {
    this.viz.branch.color = color;
    return this;
  }

  /**
   * Gets the branch path of the node.
   *
   * @returns {string}
   * @memberof TreeNode
   */
  public getBranchColor(): string {
    return this.viz.branch.color;
  }

  /**
   * Returns the width of the label of the node
   *
   * @returns {number}
   * @memberof TreeNode
   */
  public getLabelWidth(): number {
    return this.viz.label.dim.w;
  }

  /**
   * Sets the width of the label of the node
   *
   * @param {number} width
   * @memberof TreeNode
   */
  public setLabelWidth(width: number): this {
    this.viz.label.dim.w = width;
    return this;
  }

  /**
   * Returns the height of the label of the node
   *
   * @returns {number}
   * @memberof TreeNode
   */
  public getLabelHeight(): number {
    return this.viz.label.dim.h;
  }

  /**
   * Sets the height of the label of the node
   *
   * @param {number} height
   * @memberof TreeNode
   */
  public setLabelHeight(height: number): this {
    this.viz.label.dim.h = height;
    return this;
  }

  /**
   * Returns the color of the label of the node
   *
   * @returns {string}
   * @memberof TreeNode
   */
  public getDefaultLabelColor(): string {
    return this.viz.label.color;
  }

  /**
   * Sets the color of the label of the node
   *
   * @param {string} height
   * @memberof TreeNode
   */
  public setDefaultLabelColor(height: string): this {
    this.viz.label.color = height;
    this.setDisplayNameChunks(this.displayNameUserChunks);
    return this;
  }

  /**
   * Sets the font size of the label of the node.
   *
   * @param {string} fontSize
   * @memberof TreeNode
   */
  public setDefaultFontSize(fontSize: string): this {
    this.viz.label.font.size = fontSize;
    return this;
  }

  /**
   * Gets the font size of the label of the node.
   *
   * @returns {string}
   * @memberof TreeNode
   */
  public getDefaultFontSize(): string {
    return this.viz.label.font.size;
  }

  /**
   * Sets if label should be displayed or hidden
   *
   * @param {boolean} showLabel
   * @memberof TreeNode
   */
  public setShowLabel(showLabel: boolean): this {
    this.viz.label.show = showLabel;
    return this;
  }

  /**
   * Gets if display is hidden or not.
   *
   * @returns {string}
   * @memberof TreeNode
   */
  public getShowLabel(): boolean {
    return this.viz.label.show;
  }

  /**
   * Sets name to be displayed in tree in chuncks with different styles
   *
   * @param {string} displayName
   * @returns {this}
   * @memberof TreeNode
   */
  public setDisplayNameChunks(displayNameList: IDisplayNameUserChunks[]): this {
    this.displayNameUserChunks = displayNameList;
    this.displayNameChunks = displayNameList.map(displayName => {
      return {
        ...this.viz.label,
        ...displayName,
      } as IDisplayNameChunks;
    });
    return this;
  }

  /**
   * Gets the display name chunks.
   *
   * @returns {string}
   * @memberof TreeNode
   */
  public getDisplayNameChunks(): IDisplayNameChunks[] {
    return this.displayNameChunks;
  }

  /**
   * Sets name to be displayed in tree (default same as .name with default parameters)
   *
   * @param {string} displayName
   * @returns {this}
   * @memberof TreeNode
   */
  public setDisplayName(displayName: string): this {
    const simpleNameChunk = [
      {
        value: displayName,
      },
    ];
    this.setDisplayNameChunks(simpleNameChunk);
    return this;
  }

  /**
   * Gets the name to be displayed in tree.
   *
   * @returns {string}
   * @memberof TreeNode
   */
  public getDisplayName(): string {
    return this.displayNameChunks.map(d => d.value).join(' ');
  }
}
