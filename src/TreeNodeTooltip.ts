import d3 from 'd3';
import { Logger, LogLevelDescType } from 'loglevel-colored-prefix';
import { ITreeNodeTooltipItem, ITreeNodeTooltipOptions } from './interfaces';
import { Tree } from './Tree';
import { TreeNode } from './TreeNode';

const defaultOptions: ITreeNodeTooltipOptions = {
  items: ['name', 'id'],
};

class TreeNodeTooltip {
  private tree: Tree;
  private options: ITreeNodeTooltipOptions;
  private logger: Logger;
  private logLevel: LogLevelDescType;

  constructor(tree: Tree, options: ITreeNodeTooltipOptions = defaultOptions, logLevel: LogLevelDescType = 'info') {
    this.tree = tree;
    this.options = options;
    this.logLevel = logLevel;
    this.logger = new Logger(this.logLevel);
  }
}

export { TreeNodeTooltip };
