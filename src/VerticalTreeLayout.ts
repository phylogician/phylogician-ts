import * as d3 from 'd3';

import { ISvgParams } from './interfaces';
import { Tree } from './Tree';
import { TreeLayout } from './TreeLayout';

import { Logger, LogLevelDescType } from 'loglevel-colored-prefix';

export class Vertical extends TreeLayout {
  private logger: Logger;

  constructor(tree: Tree, svgParams: ISvgParams, loglevel: LogLevelDescType = 'info') {
    super(tree, svgParams);
    this.logger = new Logger(loglevel);
  }

  /**
   * Calculates all parameters of the tree
   *
   * @param {boolean} scaling
   * @returns {this}
   * @memberof Vertical
   */
  public calcTree(scaling: boolean): this {
    super.calcLabelDims();

    this.calcPosX(scaling)
      .calcPosY()
      .calcBranchPath();
    return this;
  }

  /**
   * Calculates the position X of all nodes
   *
   * @param {boolean} [scaling=false]
   * @returns {this}
   * @memberof Vertical
   */
  public calcPosX(scaling: boolean = false): this {
    const log = this.logger.getLogger('VerticalTreeLayout::calcPosX');
    const longestLabel = this.getLongestLabel();
    if (scaling) {
      const xDom = d3
        .scaleLinear()
        .domain([0, this.tree.getMaxDistanceToRoot()])
        .range([this.svgParams.margin, this.svgParams.width - this.svgParams.margin - longestLabel]);
      this.tree.nodes /*.sort((nodeA, nodeB) => nodeA.id - nodeB.pos)*/
        .forEach(node => {
          const id = node.id;
          const posX = xDom(this.tree.getDistanceToRoot(id));
          node.setPosX(posX);
        });
      // this.tree.nodes.sort((nodeA, nodeB) => nodeA.index - nodeB.index)
    } else {
      const maxHops = this.tree.getMaxHopsToRoot();
      log.debug(`Scale: ${scaling}, MaxHops: ${maxHops}`);
      const xDom = d3
        .scaleLinear()
        .domain([maxHops, 0])
        .range([this.svgParams.margin, this.svgParams.width - this.svgParams.margin - longestLabel]);
      const nodeListCounter = this.tree.getAllMaxHopsToChildLeafs();
      this.tree.nodes.forEach(node => {
        const nodeId = node.id;
        if (nodeId !== undefined) {
          log.debug(
            `Node: ${nodeId}, MaxHopsToChildLeafs: ${nodeListCounter[nodeId]}, xDom: ${xDom(nodeListCounter[nodeId])} `,
          );
          node.setPosX(xDom(nodeListCounter[nodeId]));
        }
      });
    }
    return this;
  }

  /**
   * Calculates the position Y of all nodes
   *
   * @returns {this}
   * @memberof Vertical
   */
  public calcPosY(): this {
    const log = this.logger.getLogger('VerticalTreeLayout::calcPosY');
    const leaves = this.tree.getAllLeafIds();
    const yDom = d3
      .scaleLinear()
      .domain([0, this.tree.getNumberOfLeafs() - 1])
      .range([this.svgParams.margin, this.svgParams.height - this.svgParams.margin]);
    // const spacing = (this.height - this.margin * 2) / leaves.length
    // leaves.sort((leafA, leafB) => this.tree.nodes[leafB].pos - this.tree.nodes[leafA].pos)
    leaves.forEach((leaf, i) => this.tree.nodes[leaf].setPosY(yDom(i)));
    log.debug(`Leaves: ${leaves}`);
    const internalIds = this.tree.getInternalIds().reverse();
    internalIds.forEach(internalId => {
      const internal = this.tree.nodes[internalId];
      const childrenPos: number[] = [];
      internal.children.forEach(child => {
        childrenPos.push(this.tree.nodes[child].getPosY());
      });
      internal.setPosY(childrenPos.reduce((sum, pos) => sum + pos) / childrenPos.length);
    });
    log.debug(`Nodes after calcPosY:`);
    log.debug(
      this.tree.nodes.map(n => {
        return { name: n.name, id: n.id, posY: n.getPosY() };
      }),
    );
    // this.tree.nodes.sort((nodeA, nodeB) => nodeA.index - nodeB.index)
    return this;
  }

  /**
   * Calculates the branch path
   *
   * @returns {this}
   * @memberof Vertical
   */
  public calcBranchPath(): this {
    this.tree.nodes.forEach(node => {
      const parentId = node.getParentNodeId();
      if (parentId !== null) {
        const branchCoords: Array<[number, number]> = [];
        branchCoords.push([this.tree.getNode(parentId).getPosX(), this.tree.getNode(parentId).getPosY()]);
        branchCoords.push([node.getPosX(), node.getPosY()]);
        const branch = d3
          .line()
          .x((d: any) => d[0])
          .y((d: any) => d[1])
          .curve(d3.curveStepBefore);

        node.setBranchPath(branch(branchCoords));
      }
    });
    return this;
  }

  /**
   * Helper function to place label by the corresponding node
   *
   * @param {number} id
   * @returns {string}
   * @memberof Vertical
   */
  public labelPlace(id: number): string {
    return 'translate(10)';
  }

  /**
   * Helper function to determine the anchor of text
   *
   * @param {number} id
   * @returns {string}
   * @memberof Vertical
   */
  public labelAnchor(id: number): string {
    return 'start';
  }
}
