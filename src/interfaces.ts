interface IPosViz {
  x: number;
  y: number;
  r: number;
  a: number;
}

interface IDimViz {
  h: number;
  w: number;
}

interface IFontViz {
  size: string;
  family: string;
  style: string;
}

interface ILabelViz {
  dim: IDimViz;
  font: IFontViz;
  color: string;
  show: boolean;
}

interface IDisplayNameChunks extends ILabelViz {
  value: string;
}

interface IDisplayNameUserChunks {
  value: string;
  dim?: Partial<IDimViz>;
  font?: Partial<IFontViz>;
  color?: string;
  show?: boolean;
}

interface INodeViz {
  shape: string;
  color: string;
  size: number;
}

interface IBranchViz {
  color: string;
  width: number;
  opacity: number;
  path: string;
}

interface IOptionsViz {
  node: INodeViz;
  label: ILabelViz;
  pos: IPosViz;
  branch: IBranchViz;
}

interface ISvgParams {
  width: number;
  height: number;
  margin: number;
}

interface IPhylogicianOptions {
  node: {
    hover: {
      fill: string;
      opacity: number;
      radiusMultiplier: number;
      stroke: string;
    };
    select: {
      fill: string;
      opacity: number;
      radiusMultiplier: number;
      stroke: string;
    };
  };
  transitions: {
    duration: {
      labels: number;
      branches: number;
      nodes: number;
    };
  };
}

interface ITreeNodeTooltipItem {
  name: string;
  action: () => void;
}

interface ITreeNodeTooltipOptions {
  items: Array<'name' | 'id'>;
  custom?: ITreeNodeTooltipItem[];
}

export {
  ITreeNodeTooltipItem,
  ITreeNodeTooltipOptions,
  IPhylogicianOptions,
  IOptionsViz,
  ILabelViz,
  IDisplayNameChunks,
  IDisplayNameUserChunks,
  INodeViz,
  IDimViz,
  IPosViz,
  ISvgParams,
};
