# PhylogicianTS Standards

The default tree representation is:

## Nodes

* It shows leaf nodes but not internal nodes.
* Internal nodes are transparent, but they are rendered so they can be clickable.
  
## Labels

* Labels of leafs are rendered by default.
* Internal labels are set to display `hidden`
  
