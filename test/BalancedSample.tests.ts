// tslint:disable: no-unused-expression
import { expect } from 'chai'
import fs from 'fs'
import performance from 'performance-now'
import { BalancedSample } from '../src/BalancedSample'
import { Tree } from '../src/Tree'

const levelLog = 'info'

const largeTree = fs.readFileSync('data/bac.tree').toString()

describe.skip('BalancedSample', () => {
  describe('pick', () => {
    it('should work', () => {
      const nwk = '((A2lice:2,D3ebbie:3)1N:4,((B6ob:3,C7harlie:4,((E10llen:3,F11elix:4)N9:2,G12il:5)N8:3)N5:2,I13lly:5)N4:3)0N;'
      const tree = new Tree(levelLog)
      tree.buildTree(nwk)
      const bsamp = new BalancedSample(tree, 'warn')
      const rounds = 10000
      const rootId = 0
      const numberOfPicks = 4
      const picked: {[id:number]: number} = {
          2: 0,
          3: 0,
          6: 0,
          7: 0,
          10: 0,
          11: 0,
          12: 0,
          13: 0
      }
      for (let i = 0; i < rounds; i++) {         
          const picks = bsamp.pick([], rootId, numberOfPicks, false, i.toString())
          // console.log(picks)
          picks.forEach((pick) => {
              if (picked[pick]) {
                  picked[pick] += 1/rounds
              }
              else {
                  picked[pick] = 1/rounds
              }
          })
      }
      const expected: {[id:number]: number} = {
        '2': 0.49929999999996133,
        '3': 0.4966999999999616,
        '6': 0.48989999999996237,
        '7': 0.5034999999999609,
        '10': 0.4904999999999623,
        '11': 0.5120999999999599,
        '12': 0.5039999999999608,
        '13': 0.5039999999999608
      }
      // console.log(picked)
      const tolerance = 0.02
      const leafs = tree.getAllLeafIds()
      leafs.forEach((leaf) => {
          expect(picked[leaf]).to.exist
          expect(expected[leaf]).to.exist
          const error = Math.abs(picked[leaf] - expected[leaf])
          expect(error).to.be.lessThan(tolerance)
      })
    })
    describe('working with large trees', () => {
      it('should not be too slow with large trees', () => {
          const expectedTime = 10000
          const tree = new Tree(levelLog)
          tree.buildTree(largeTree)
          const bsamp = new BalancedSample(tree, 'debug')
          const rootId = 0
          const numberOfPicks = 10
          const startClock = performance()
          const picks = bsamp.pick([], rootId, numberOfPicks)
          const elapsed = performance() - startClock
          expect(elapsed).to.be.lessThan(expectedTime)
          expect(picks.length).eql(numberOfPicks)
      })
    })
  })
  describe('pickMixed', () => {
    it('should work', () => {
      const nwk = '((A2lice:2,D3ebbie:3,A2lice:2,D3ebbie:3,A2lice:2,D3ebbie:3,A2lice:2,D3ebbie:3,A2lice:2,D3ebbie:3,A2lice:2,D3ebbie:3)1N:4,((B6ob:3,C7harlie:4,((E10llen:3,F11elix:4)N9:2,G12il:5)N8:3)N5:2,I13lly:5)N4:3)0N;'
      const tree = new Tree(levelLog)
      tree.buildTree(nwk)
      const bsamp = new BalancedSample(tree)
      const rounds = 1000
      const rootId = 0
      const numberOfPicks = 4
      const picked: {[id:number]: number} = {
        '2': 0,
        '3': 0,
        '4': 0,
        '5': 0,
        '6': 0,
        '7': 0,
        '8': 0,
        '9': 0,
        '10': 0,
        '11': 0,
        '12': 0,
        '13': 0,
        '16': 0,
        '17': 0,
        '20': 0,
        '21': 0,
        '22': 0,
        '23': 0
      }
      for (let i = 0; i < rounds; i++) {         
          const picks = bsamp.pickMixed([], rootId, numberOfPicks, false, i.toString())
          console.log(picks)
          const set = new Set(picks)
          expect(set.size, `Run with seed ${i} picked the same leaf more than once: ${JSON.stringify(picks)}`).eql(picks.length)
          expect(set.size, `Run with seed ${i} did not pick enough samples: ${JSON.stringify(picks)}`).eql(numberOfPicks)
          picks.forEach((pick) => {
              if (picked[pick]) {
                  picked[pick] += 1/rounds
              }
              else {
                  picked[pick] = 1/rounds
              }
          })
      }
      const expected: {[id:number]: number} = {
        '2': 0.25200000000000017,
        '3': 0.22200000000000017,
        '4': 0.2490000000000002,
        '5': 0.25600000000000017,
        '6': 0.25500000000000017,
        '7': 0.2450000000000002,
        '8': 0.2430000000000002,
        '9': 0.25100000000000017,
        '10': 0.23400000000000018,
        '11': 0.22900000000000018,
        '12': 0.22800000000000017,
        '13': 0.008,
        '16': 0.3100000000000002,
        '17': 0.1550000000000001,
        '20': 0.22400000000000017,
        '21': 0.22400000000000017,
        '22': 0.21200000000000016,
        '23': 0.20300000000000015
      }
      console.log(picked)
      const tolerance = 0.02
      const leafs = tree.getAllLeafIds()
      leafs.forEach((leaf) => {
          expect(picked[leaf]).to.exist
          expect(expected[leaf]).to.exist
          const error = Math.abs(picked[leaf] - expected[leaf])
          expect(error).to.be.lessThan(tolerance)
      })
    })
  })
  describe('setScores', () => {
    it('should not be too slow with large trees', function() {
      this.timeout(10000)
      const expectedTime = 10000
      const tree = new Tree(levelLog)
      tree.buildTree(largeTree)
      const bsamp = new BalancedSample(tree, 'debug')
      const startClock = performance()
      bsamp.setScores()
      const elapsed = performance() - startClock
      expect(elapsed).to.be.lessThan(expectedTime)
    })
  })
})