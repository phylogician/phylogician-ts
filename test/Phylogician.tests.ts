// tslint:disable: no-unused-expression
import { expect } from 'chai'
import puppeteer from 'puppeteer'
import { ISvgParams } from '../src/interfaces'
import { Tree } from '../src/Tree'

const loglevel = 'debug'
const layout = 'vertical'
const newick = '((Alice:2,Debbie:3):4,((Bob:3,Charlie:4):2,Jack:5):3);'
const challenge = '((Alice, Debbie), Bob, Charlie);'
const tolerance = 5

const opts = {
  args: ['--start-maximized', '--window-size=1920,1040'],
  devtools: false,
  headless: true,
  timeout: 0,
}

describe('Phylogician', () => {
  let browser: any
  const pageSize = {
    height: 1000,
    width: 1000
  }
  after ( () => {
    browser.close();
  });
  before(async function () {
    this.timeout(15000)
    browser = await puppeteer.launch(opts)
  })
  describe('setSvgParameters with VerticalTreeLayout', () => {
    let expected: any
    let newExpected: any
    let svgParams: any
    let newSvgParams: any
    let posYList: any
    let posYListBranch: any
    before(async function () {
      this.timeout(15000)
      const page = await browser.newPage();
      await page.setViewport(pageSize)
      await page.setContent(`
        <html>
          <head></head>
          <body>
            <h1><center>PhylogicianJS development environment.</center></h1>
            <h3><center>Testing changes in width and height should accomodate tree</center></h3>
            <button id='changeSize' onclick="clicked()">Change Size</button>
            <button id='makeTree' onclick="makeTree()">makeTree</button>
            <div id='TreeDiv'></div>
          </body></html>`)
      await page.addStyleTag({
        content: `
        body {
            margin-top: 30px;
        }
        #TreeDiv {
            width: 800px;
            height: 500px;
            border-width: 1px;
            border-color: black;
            border-style: solid;
  
            position:absolute; /*it can be fixed too*/
            left:0; right:0;
            top:0; bottom:0;
            margin:auto;
        }`
      })
      await page.addScriptTag({
        path: './lib/index.browser.js'
      })
      await page.addScriptTag({
        content: `
          const phyloTree = new Phylogician.Phylogician(undefined, '${loglevel}')
          const svgParam = {
            width: 100
          }
          const newSvgParams = {
            width: 300,
            height: 200
          }
          function makeTree () {
              phyloTree
                .data("${newick}")
                .setSvgParameters(svgParam)
                .setLayout('${layout}')
                .draw("#TreeDiv")
          }
          function clicked () {
              phyloTree.setSvgParameters(newSvgParams)
          }`
      })
      await page.click('#makeTree')
      await page.waitFor(2000)
      page.on('pageerror', (err: any) => console.log(err));
      svgParams = await page.evaluate(() => {
        const svg = document.getElementById('phylogicianTS')
        const params: ISvgParams = {
          height: 0,
          margin: 20,
          width: 0
        }
        if (svg) {
          const width = svg.getAttribute('width')
          if (width) {
            params.width = parseFloat(width)
          }
          const height = svg.getAttribute('height')
          if (height) {
            params.height = parseFloat(height)
          }
        }
        return params
      })  
      expected = {
        height: 500,
        margin: 20,
        width: 100
      }
      await page.click('#changeSize')
      await page.waitFor(1100)
      newSvgParams = await page.evaluate(() => {
        const svg = document.getElementById('phylogicianTS')
        const params: ISvgParams = {
          height: 0,
          margin: 20,
          width: 0
        }
        if (svg) {
          const width = svg.getAttribute('width')
          if (width) {
            params.width = parseFloat(width)
          }
          const height = svg.getAttribute('height')
          if (height) {
            params.height = parseFloat(height)
          }
        }
        return params
      })  
      newExpected = {
        height: 200,
        margin: 20,
        width: 300
      }
      const tree = new Tree()
      tree.buildTree(newick)
      const nodeSvgIds = tree.getAllLeafIds().map((n: number) => `phylogicianTS-node-${tree.nodes[n].id}`)
      const nodeElements = []
      for (const nodeSvgId of nodeSvgIds) {
        const cy = await page.evaluate((nodeId: string) => {
          const element = document.getElementById(nodeId)
          if(element) {
            return element.getAttribute('cy')
          }
        }, nodeSvgId)
        nodeElements.push(cy)
      }
      posYList = nodeElements.map((element) => {
        if (element) {
          return parseFloat(element)
        }
        return Math.random()
      })
      const branchSvgIds = tree.getAllLeafIds().map((n: number) => `phylogicianTS-branch-${tree.nodes[n].id}`)
      const branchElements = []
      for (const branchSvgId of branchSvgIds) {
        const cy = await page.evaluate((branchId: string) => {
          const element = document.getElementById(branchId)
          if(element) {
            const path = element.getAttribute('d')
            if (path) {
              const pathSplit = path.split(',')
              return pathSplit[pathSplit.length - 1]
            }
          }
        }, branchSvgId)
        branchElements.push(cy)
      }
      posYListBranch = branchElements.map((element) => {
        if (element) {
          return parseFloat(element)
        }
        return Math.random()
      })
    })
    it('should set the parameters and change the size of the svg in DOM before drawing', () => {
      expect(svgParams).eql(expected)
    })
    it('should resize the svg after the tree is drawn', () => {
      expect(newSvgParams).eql(newExpected)
    })
    it('nodes should be placed correctly', () => {
      expect(posYList).not.empty
      posYList.forEach((y: number, i: number) => {
        expect(y - posYListBranch[i], `Error in position ${i}`).to.be.lessThan(tolerance * 2 + 5)
      })
    })
    it('should update the tree representation')
  })
  describe('setFontSizeAllLabels', () => {
    let page: any
    before(async function () {
      this.timeout(15000)
      page = await browser.newPage();
      await page.setViewport(pageSize)
      await page.setContent(`<html><head></head><body><h1><center>PhylogicianJS development environment.</center></h1><div id='TreeDiv'></div><button id='changeFont' onclick="clicked()">Change Font</button><button id='makeTree' onclick="makeTree()">makeTree</button></body></html>`)
      await page.addStyleTag({
        content: `
        body {
            margin-top: 30px;
        }
        #TreeDiv {
            width: 800px;
            height: 500px;
            border-width: 1px;
            border-color: black;
            border-style: solid;
            margin:auto;
        }`
      })
      await page.addScriptTag({
        path: './lib/index.browser.js'
      })
      await page.addScriptTag({
        content: `
          const fontSize = '30px'
          const phyloTree = new Phylogician.Phylogician(undefined, '${loglevel}')
          function makeTree () {
              phyloTree
                .data("${newick}")
                .setLayout('${layout}')
                .draw("#TreeDiv")
          }
          function clicked () {
              phyloTree.setFontSizeAllLabels(fontSize)
          }`
      })
      await page.click('#makeTree')
      await page.waitFor(2000)
      page.on('pageerror', (err: any) => { throw err });
    })
    describe('before changing the font', () => {
      it('The node should have the right innerHTML with the node\'s name', async () => {
        const name = await page.evaluate(() => {
          const alice = document.getElementById('phylogicianTS-label-2')
          if (alice) {
            return alice.innerHTML
          }
        })
        expect(name).eql('<tspan font-size="16px" font-style="normal" fill="black">Alice</tspan>')
      })
      it('Should have the label in the right X position', async () => {
        const expectedPos = 494
        const posX = await page.evaluate(() => {
          const alice = document.getElementById('phylogicianTS-label-2')
          if (alice) {
            return alice.getAttribute('x')
          }
        })
        const errorX = Math.abs(expectedPos - posX)
        expect(errorX).be.lessThan(tolerance)
      })
      it('Should have the label in the right Y position', async () => {
        const expectedPos = 24.25
        const posY= await page.evaluate(() => {
          const alice = document.getElementById('phylogicianTS-label-2')
          if (alice) {
            return alice.getAttribute('y')
          }
        })
        const errorY = Math.abs(expectedPos - posY)
        expect(errorY).be.lessThan(tolerance)
      })
    })
    describe('after change font size', () => {
      before(async () => {
        await page.click('#changeFont')
        await page.waitFor(1100)
      })
      it('Still have the right innerHTML with the node\'s name', async () => {
        const name = await page.evaluate(() => {
          const alice = document.getElementById('phylogicianTS-label-2')
          if (alice) {
            return alice.innerHTML
          }
        })
        expect(name).eql('<tspan font-size="30px" font-style="normal" fill="black">Alice</tspan>')
      })
      it('Should have the label in the right X position', async () => {
        const expectedPos = 467
        const posX = await page.evaluate(() => {
          const alice = document.getElementById('phylogicianTS-label-2')
          if (alice) {
            return alice.getAttribute('x')
          }
        })
        const errorX = Math.abs(expectedPos - posX)
        expect(errorX).be.lessThan(tolerance)
      })
      it('Should have the label in the right Y position', async () => {
        const expectedPos = 28
        const posY= await page.evaluate(() => {
          const alice = document.getElementById('phylogicianTS-label-2')
          if (alice) {
            return alice.getAttribute('y')
          }
        })
        const errorY = Math.abs(expectedPos - posY)
        expect(errorY).be.lessThan(tolerance)
      })
    })
  })
  describe('VerticalTreeLayout basics', () => {
    let page: any
    let originalNodesPosition: any
    before(async function () {
      this.timeout(15000)
      page = await browser.newPage();
      await page.setViewport(pageSize)
      await page.setContent(`
        <html>
          <head></head>
          <body>
            <h2><center>PhylogicianJS test environment.</center></h2>
            <p>Layout: Vertical</p>
            <p>Testing: <font color="red"><span id="test" /></font><p>
            <button id='scale' onclick="clicked()">scale</button>
            <button id='changeNodeColor' onclick="changeNodeColor()">Change node color</button>
            <button id='changeBranchColor' onclick="changeBranchColor()">Change branch color</button>
            <button id='changeChildrenBranchColor' onclick="changeChildrenBranchColor()">Change children branch color</button>
            <button id='showLeafLabels' onclick="showLeafLabels()">hideLeafLabels</button>
            <button id='ladderize' onclick="ladderize(0)">Ladderize</button>
            <button id='makeTree' onclick="makeTree()">makeTree</button>
            <button id='markNodes' onclick="markNodes()">Mark nodes</button>
            <button id='changeLabelColor' onclick="changeLabelColor()">Change label color</button>
            <button id='changeDisplayName' onclick="changeDisplayName()">Change display name</button>
            <button id='changeDisplayNameChunks' onclick="changeDisplayNameChunks()">Change name chunks</button>
            <div id='TreeDiv'></div>
          </body>
        </html>`)
      await page.addStyleTag({
          content: `
          body {
              margin-top: 30px;
          }
          #TreeDiv {
              width: 800px;
              height: 500px;
              border-width: 1px;
              border-color: black;
              border-style: solid;

              left:0; right:0;
              top:0; bottom:0;
              margin:auto;
          }`
      })
      await page.addScriptTag({
        path: './lib/index.browser.js'
      })
      await page.addScriptTag({
          content: `
              const phyloTree = new Phylogician.Phylogician(undefined, '${loglevel}')
              let lad = 0
              let show = false
              function makeTree () {
                document.getElementById("test").innerHTML = 'draw()'
                phyloTree
                  .data("${newick}")
                  .setLayout('${layout}')
                  .draw("#TreeDiv")
              }
              function clicked () {
                document.getElementById("test").innerHTML = 'toggleScale()'
                phyloTree.toggleScale()
              }
              function showLeafLabels () {
                document.getElementById("test").innerHTML = 'showLeafLabels()'
                phyloTree.showLeafLabels(show)
                show = !show
              }
              function ladderize() {
                lad++
                const root = phyloTree.tree.getRootNode()
                document.getElementById("test").innerHTML = 'ladderize(' + root.id + ') - ' + lad
                phyloTree.ladderize(root.id)
              }
              function changeNodeColor() {
                document.getElementById("test").innerHTML = 'changeNodeColor()'
                const node = phyloTree.tree.nodes[3]
                node.setNodeColor('rgb(255, 0, 0)')
                phyloTree.update()
              }
              function changeBranchColor() {
                document.getElementById("test").innerHTML = 'changeBranchColor()'
                const node = phyloTree.tree.nodes[3]
                node.setBranchColor('rgb(255, 0, 0)')
                phyloTree.update()
              }
              function changeChildrenBranchColor() {
                document.getElementById("test").innerHTML = 'changeChildrenBranchColor()'
                phyloTree.tree.setChildrenBranchColor(4, 'rgb(0, 128, 0)')
                phyloTree.update()
              }
              function markNodes() {
                document.getElementById("test").innerHTML = 'markNode()'
                const nodes = [2, 7]
                document.getElementById("test").innerHTML = 'markNodes()'
                phyloTree.markNodes(nodes, 'rgb(0, 128, 0)')
              }
              function changeLabelColor() {
                document.getElementById("test").innerHTML = 'changeLabelColor()'
                const node = phyloTree.tree.nodes[2]
                node.setDefaultLabelColor('rgb(255, 0, 0)')
                phyloTree.update()
              }
              function changeDisplayName() {
                document.getElementById("test").innerHTML = 'changeDisplayName()'
                const node = phyloTree.tree.nodes[7]
                node.setDisplayName('Charlie Parker')
                phyloTree.update()
              }
              function changeDisplayNameChunks() {
                document.getElementById("test").innerHTML = 'changeDisplayNameChunks()'
                const node = phyloTree.tree.nodes[7]
                nameChunks = [
                  {
                    value: 'Charlie'
                  },
                  {
                    value: ' '
                  },
                  {
                    value: 'Parker',
                    font: {
                      style: 'italic'
                    }
                  }
                ]
                node.setDisplayNameChunks(nameChunks)
                console.log('testing chunks')
                console.log(node.getDisplayNameChunks())
                phyloTree.update()
              }
              `
      })
      await page.click('#makeTree')
      await page.waitFor(2000)
      page.on('pageerror', (err: any) => { throw err });
      originalNodesPosition = await page.evaluate(() => {
        const nodes = document.querySelectorAll('.node')
        const list: any = []
        nodes.forEach((node) => {
          const xStr = node.getAttribute('cx')
          const yStr = node.getAttribute('cy')
          if (xStr && yStr) {
            const coord = {
              x: parseFloat(xStr) || Math.random(),
              y: parseFloat(yStr) || Math.random()
            }
            list.push(coord)
          }
        })
        return list
      })
    });
    describe('Labels', () => {
      it('should have labels of leafs with attribute display: block', async () => {
        const tree = new Tree()
        tree.buildTree(newick)
        const nodeSvgIds = tree.getAllLeafIds().map((n: number) => `phylogicianTS-label-${tree.nodes[n].id}`)
        const elements = []
        for (const nodeSvgId of nodeSvgIds) {
          const cy = await page.evaluate((nodeId: string) => {
            const element = document.getElementById(nodeId)
            if(element) {
              return element.getAttribute('display')
            }
          }, nodeSvgId)
          elements.push(cy)
        }
        expect(elements).not.be.empty
        for (let i: number = 0; i < elements.length -1 ; i++) {
          expect(elements[i]).eql('block')
        }
      })
      it('It should have draw Alice\'s label', async () => {
        const expectedBBox =  { width: 34.78125, height: 17 }
        const alice = await page.$('#phylogicianTS-label-2')
        expect(alice).exist
        if (alice) {
            const bBox = await alice.boundingBox()
            expect(bBox).exist
            if (bBox) {
              const errorX = Math.abs(expectedBBox.width - bBox.width)
              const errorY = Math.abs(expectedBBox.height - bBox.height)
              expect(errorX).be.lessThan(tolerance)
              expect(errorY).be.lessThan(tolerance)
            }
        }
      })
      it('It should have draw Charlie\'s label', async () => {
        const expectedBBox =  { width: 47.09375, height: 17 }
        const charlie = await page.$('#phylogicianTS-label-7')
        expect(charlie).exist
        if (charlie) {
            const bBox = await charlie.boundingBox()
            expect(bBox).exist
            if (bBox) {
              const errorX = Math.abs(expectedBBox.width - bBox.width)
              const errorY = Math.abs(expectedBBox.height - bBox.height)
              expect(errorX).be.lessThan(tolerance)
              expect(errorY).be.lessThan(tolerance)
            }
        }
      })
      describe('change label color', () => {
        before(async() => {
          await page.click('#changeLabelColor')
          await page.waitFor(1100)
        })
        it('should have changed the color of the label', async () => {
          const AliceLabelColor = await page.evaluate(() => {
            const AliceN = document.getElementById('phylogicianTS-label-2')
            if(AliceN) {
                return AliceN.getElementsByTagName('tspan')[0].getAttribute('fill')
            }
          })
          expect(AliceLabelColor).to.exist
          const AliceExpectedLabelColor = 'rgb(255, 0, 0)'
          expect(AliceLabelColor).eql(AliceExpectedLabelColor)
        })
      })
      describe('change display name', () => {
        before(async() => {
          await page.click('#changeDisplayName')
          await page.waitFor(1100)
        })
        it('should have changed the name of the node', async () => {
          const charlieDisplayName = await page.evaluate(() => {
            const charlieN = document.getElementById('phylogicianTS-label-7')
            if(charlieN) {
                return charlieN.getElementsByTagName('tspan')[0].innerHTML
            }
          })
          expect(charlieDisplayName).to.exist
          const charlieExpectedDisplayName = 'Charlie Parker'
          expect(charlieDisplayName).eql(charlieExpectedDisplayName)
        })
        it('It should have draw Charlie Parker\'s label and re-adjust the tree', async () => {
          const expectedBBox =  { width: 47.09375, height: 17 }
          const charlie = await page.$('#phylogicianTS-label-7')
          expect(charlie).exist
          if (charlie) {
              const bBox = await charlie.boundingBox()
              expect(bBox).exist
              const CharlieX = await page.evaluate(() => {
                const CharlieN = document.getElementById('phylogicianTS-label-7')
                if(CharlieN) {
                    return CharlieN.getAttribute('x')
                }
              })
              expect(CharlieX).exist
              const charlieX = parseFloat(CharlieX)
              if (bBox) {
                expect(bBox.width + charlieX).be.lessThan(800)
              }
          }
        })
      })
      describe('display name in chunks', () => {
        before(async() => {
          await page.click('#changeDisplayNameChunks')
          await page.waitFor(1100)
        })
        it('should have changed the name of the node', async () => {
          const charlieDisplayName = await page.evaluate(() => {
            const charlieN = document.getElementById('phylogicianTS-label-7')
            if(charlieN) {
                return charlieN.getElementsByTagName('tspan')
            }
          })
          expect(charlieDisplayName).to.exist
          expect(Object.keys(charlieDisplayName)).to.be.of.length(3)
          const charlieExpectedDisplayName = 'Charlie Parker'
          expect(charlieDisplayName[2].__data__.font.style, 'Not changing to italic').eql('italic')
          const fullName = Object.keys(charlieDisplayName).map(key => charlieDisplayName[key].__data__.value).join('')
          expect(fullName).eql(charlieExpectedDisplayName)
        })
      })
    })
    describe('Node position and style', () => {
      it('It should have draw the nodes with the right radius', async () => {
        const aliceRealNodeRadius = await page.evaluate(() => {
            const aliceN = document.getElementById('phylogicianTS-node-2')
            if(aliceN) {
                return parseInt(aliceN.getAttribute('r') || '', 10)
            }
        })
        const aliceExpectedNodeRadius = 3
        expect(aliceRealNodeRadius).eql(aliceExpectedNodeRadius)
      })
      it('It should have all the y positions of leafs equidistant from each other', async () => {
        const tree = new Tree()
        tree.buildTree(newick)
        const nodeSvgIds = tree.getAllLeafIds().map((n: number) => `phylogicianTS-node-${tree.nodes[n].id}`)
        const elements = []
        for (const nodeSvgId of nodeSvgIds) {
          const cy = await page.evaluate((nodeId: string) => {
            const element = document.getElementById(nodeId)
            if(element) {
              return element.getAttribute('cy')
            }
          }, nodeSvgId)
          elements.push(cy)
        }
        const posYList = elements.map((element) => {
          if (element) {
            return parseInt(element, 10)
          }
          return 0
        })
        expect(posYList).not.be.empty
        const interval = posYList[0] - posYList[1]
        expect(interval).not.eql(0)
        for (let i: number = 1; i < posYList.length - 2; i++) {
          expect(posYList[i] - posYList[i+1]).eql(interval)
        }
      })
    })
    describe('toggleScale', () => {
      before(async() => {
        await page.click('#scale')
        await page.waitFor(1100)
      })
      it('It should have draw all leafs at the same x position after descaling', async () => {
        const tree = new Tree()
        tree.buildTree(newick)
        const nodeSvgIds = tree.getAllLeafIds().map((n) => `phylogicianTS-node-${tree.nodes[n].id}`)
        const elements = []
        for (const nodeSvgId of nodeSvgIds) {
          const cx = await page.evaluate((nodeId: string) => {
            const element = document.getElementById(nodeId)
            if(element) {
              return element.getAttribute('cx')
            }
          }, nodeSvgId)
          elements.push(cx)
        }
        const posXList = elements.map((element) => {
          if (element) {
            return parseInt(element, 10)
          }
          return Math.random()
        })
        expect(posXList.length, 'This test failed because there is no elements in the list').to.not.eql(0)
        const interval = posXList[0] - posXList[1]
        expect(interval).to.be.lessThan(tolerance)
        for (let i: number = 1; i < posXList.length - 2; i++) {
          expect(posXList[i] - posXList[i+1]).to.be.lessThan(tolerance)
        }
      })
    })
    describe('showLeafLabels', () => {
      before(async() => {
        await page.click('#showLeafLabels')
        await page.waitFor(1100)
      })
      it('should have labels with attribute display: none', async () => {
        const tree = new Tree()
        tree.buildTree(newick)
        const nodeSvgIds = tree.getAllLeafIds().map((n: number) => `phylogicianTS-label-${tree.nodes[n].id}`)
        const elements = []
        for (const nodeSvgId of nodeSvgIds) {
          const labelShow = await page.evaluate((nodeId: string) => {
            const element = document.getElementById(nodeId)
            if(element) {
              return element.getAttribute('display')
            }
          }, nodeSvgId)
          elements.push(labelShow)
        }
        expect(elements).not.be.empty
        for (let i: number = 0; i < elements.length -1 ; i++) {
          expect(elements[i]).eql('none')
        }
      })
      it('should recalculate the tree to ocupy the whole space', async () => {
        const newNodesPosition = await page.evaluate(() => {
          const nodes = document.querySelectorAll('.node')
          const list: any = []
          nodes.forEach((node) => {
            const xStr = node.getAttribute('cx')
            const yStr = node.getAttribute('cy')
            if (xStr && yStr) {
              const coord = {
                x: parseFloat(xStr) || Math.random(),
                y: parseFloat(yStr) || Math.random()
              }
              list.push(coord)
            }
          })
          return list
        })
        expect(newNodesPosition).not.eql(originalNodesPosition)
      })
    })
    describe('ladderize', () => {
      before(async() => {
        await page.click('#showLeafLabels')
        await page.click('#ladderize')
        await page.waitFor(1100)
      })
      it('It should have ladderized the whole tree', async () => {
        const tree = new Tree()
        tree.buildTree(newick)
        const nodeSvgIds = tree.getAllLeafIds().map((n) => `phylogicianTS-node-${tree.nodes[n].id}`)
        const elements = []
        for (const nodeSvgId of nodeSvgIds) {
          try {
            const cy = await page.evaluate((nodeId: string) => {
              const element = document.getElementById(nodeId)
              if(element) {
                return element.getAttribute('cy')
              }
            }, nodeSvgId)
            elements.push(cy)
          } catch (err) {
            throw err
          }
        }
        const posYList = elements.map((element) => {
          if (element) {
            return parseInt(element, 10)
          }
          return Math.random()
        })
        const expectedPosYList = [ 364, 479, 20, 135, 250 ]
        expect(posYList).not.empty
        posYList.forEach((y: number, i: number) => {
          expect(y - expectedPosYList[i]).to.be.lessThan(tolerance)
        })
      })
      it('It should have reverse ladderized the whole tree', async () => {
        await page.click('#ladderize')
        await page.waitFor(1100)
        const tree = new Tree()
        tree.buildTree(newick)
        const nodeSvgIds = tree.getAllLeafIds().map((n) => `phylogicianTS-node-${tree.nodes[n].id}`)
        const elements = []
        for (const nodeSvgId of nodeSvgIds) {
          try {
            const cy = await page.evaluate((nodeId: string) => {
              const element = document.getElementById(nodeId)
              if(element) {
                return element.getAttribute('cy')
              }
            }, nodeSvgId)
            elements.push(cy)
          } catch (err) {
            throw err
          }
        }
        const posYList = elements.map((element) => {
          if (element) {
            return parseInt(element, 10)
          }
          return Math.random()
        })
        const expectedPosYList = [20, 135, 365, 480, 250]
        expect(posYList).not.empty
        posYList.forEach((y: number, i: number) => {
          expect(y - expectedPosYList[i]).to.be.lessThan(tolerance)
        })
      })
    })
    describe('change node color', () => {
      before(async() => {
        await page.click('#changeNodeColor')
        await page.waitFor(1100)
      })
      it('should have changed the color of the node', async () => {
        const bobNodeColor = await page.evaluate(() => {
          const bobN = document.getElementById('phylogicianTS-node-3')
          if(bobN) {
              return bobN.getAttribute('fill')
          }
        })
        expect(bobNodeColor).to.exist
        const bobExpectedNodeColor = 'rgb(255, 0, 0)'
        expect(bobNodeColor).eql(bobExpectedNodeColor)
      })
    })
    describe('change branch color', () => {
      before(async() => {
        await page.click('#changeBranchColor')
        await page.waitFor(1100)
      })
      it('should have changed the color of the branch', async () => {
        const bobNodeColor = await page.evaluate(() => {
          const bobN = document.getElementById('phylogicianTS-branch-3')
          if(bobN) {
              return bobN.getAttribute('stroke')
          }
        })
        expect(bobNodeColor).to.exist
        const bobExpectedNodeColor = 'rgb(255, 0, 0)'
        expect(bobNodeColor).eql(bobExpectedNodeColor)
      })
    })
    describe('change the color of all children branches', () => {
      before(async() => {
        await page.click('#changeChildrenBranchColor')
        await page.waitFor(1100)
      })
      it('should have changed the color of the branch', async () => {
        const childrenOfFour = [5, 6, 7 ,8 ]
        const green = 'rgb(0, 128, 0)'
        const red = 'rgb(255, 0, 0)'
        const expectedColors = ['black', 'black', 'black', red , 'black', green, green, green, green]
        const branchColors = await page.evaluate(() => {
          const branches = document.querySelectorAll('.branch')
          const list: string[] = []
          if (branches) {
            branches.forEach(branch => {
              const color = branch.getAttribute('stroke')
              color && list.push(color)
            })
          }
          return list
        })
        expect(branchColors).eql(expectedColors)
      })
    })
    describe('markNodes', () => {
      before(async() => {
        await page.click('#markNodes')
        await page.waitFor(1100)
      })
      it('should change the colors of nodes 2 and 7 for green', async () => {
        const node1NodeColor = await page.evaluate(() => {
          const item = document.getElementById('phylogicianTS-node-2')
          if(item) {
              return item.getAttribute('fill')
          }
        })
        const node2NodeColor = await page.evaluate(() => {
          const item = document.getElementById('phylogicianTS-node-7')
          if(item) {
              return item.getAttribute('fill')
          }
        })
        const expectedNodeColor = 'rgb(0, 128, 0)'
        expect(node1NodeColor).to.exist
        expect(node1NodeColor).eql(expectedNodeColor)
        expect(node2NodeColor).to.exist
        expect(node2NodeColor).eql(expectedNodeColor)
      })
    })
  })
  describe('VerticalTreeLayout bug fix #11', () => {
    let page: any
    let nodePositionList: any
    let nodePositionListScaled: any
    before(async function() {
      this.timeout(15000)
      page = await browser.newPage();
      await page.setViewport(pageSize)
      await page.setContent(`
        <html>
          <head></head>
          <body>
            <h1><center>PhylogicianJS development environment.</center></h1>
            <div id='TreeDiv'></div>
            <button id='scale' onclick="clicked()">scale</button>
            <button id='makeTree' onclick="makeTree()">makeTree</button>
          </body>
        </html>`)
      await page.addStyleTag({
        content: `
        body {
            margin-top: 30px;
        }
        #TreeDiv {
            width: 800px;
            height: 500px;
            border-width: 1px;
            border-color: black;
            border-style: solid;
            margin:auto;
        }`
      })
      await page.addScriptTag({
        path: './lib/index.browser.js'
      })
      await page.addScriptTag({
        content: `
          const fontSize = '30px'
          const phyloTree = new Phylogician.Phylogician(undefined, '${loglevel}')
          function makeTree () {
              phyloTree
                .data("${challenge}")
                .setLayout('${layout}')
                .draw("#TreeDiv")
          }
          function clicked () {
              phyloTree.toggleScale()
          }`
      })
      await page.click('#makeTree')
      await page.waitFor(2000)
      page.on('pageerror', (err: any) => { throw err });
      nodePositionList = await page.evaluate(() => {
        const nodes = document.querySelectorAll('.node')
        const list: any = []
        nodes.forEach((node) => {
          const xStr = node.getAttribute('cx')
          const yStr = node.getAttribute('cy')
          if (xStr && yStr) {
            const coord = {
              x: parseFloat(xStr) || Math.random(),
              y: parseFloat(yStr) || Math.random()
            }
            list.push(coord)
          }
        })
        return list
      })
      await page.click('#scale')
      await page.waitFor(1100)
      nodePositionListScaled = await page.evaluate(() => {
        const nodes = document.querySelectorAll('.node')
        const list: any = []
        nodes.forEach((node) => {
          const xStr = node.getAttribute('cx')
          const yStr = node.getAttribute('cy')
          if (xStr && yStr) {
            const coord = {
              x: parseFloat(xStr) || Math.random(),
              y: parseFloat(yStr) || Math.random()
            }
            list.push(coord)
          }
        })
        return list
      })
    })
    it('all nodes should line up with this tree with scaling (no branch lengths)', async () => {
      let xValue = 0
      expect(nodePositionList).not.empty
      nodePositionList.forEach((coord: { x: number }, i: number) => {
        if (i === 0) {
          xValue = coord.x
        }
        else {
          expect(xValue).eql(coord.x)
        }
      })
    })
    it('Not all nodes should line up with this tree without scaling - topology only (no branch lengths)', async () => {
      let xValue = 0
      expect(nodePositionListScaled).not.empty
      nodePositionListScaled.forEach((coord: { x: number }, i: number) => {
        if (i === 0) {
          xValue = coord.x
        }
        else if (i === 1) {
          expect(xValue).not.eql(coord.x)
        }
      })
    })
  })
  describe('Ladderize bug fix #26', () => {
    let page: any
    let nodePositionList: any
    let nodePositionListLadderized: any
    const complexTree = '(Rh_per|GCF_000382165.1-H141_RS0111605:0.15110752375429112315,(((((((((Sa_sha|GCF_000215955.2-SSPSH_RS15705:0.87419220310435707955,((Gi_api|GCF_000599985.1-GAPWK_RS12960:0.30672502546257390810,Fr_per|GCF_000807275.1-FPB0191_RS10290:0.30983578673370937295)100:1.10619510105485185925,(((Pl_ger|GCF_000757785.1-LG71_RS13740:0.12671342473269528450,Fr_pul|GCF_000621185.1-Q370_RS0101925:0.09790825799542554608)100:0.12517539486502773927,Xe_dou|GCF_000968195.1-XDD1_RS10665:0.23913905141549529620)100:0.15948416275986720958,Pl_ger|GCF_000757785.1-LG71_RS07410:0.41104394167262958693)90:0.22719977887063710908)100:0.27470042694854696075)100:1.20105555967632593450,(Bd_bac|GCF_000196175.1-BD_RS15235:1.93459186075526656801,((((Le_sao|GCF_001465875.1-BN3124_RS10910:0.28264105214317319925,Fl_dum|GCF_000236165.1-KYQ_RS06510:0.18563006279757618411)90:0.13306084252105129795,Le_pne|GCF_000008485.1-lpg1792:0.37777868894490873597)90:0.21899525628810442845,(Le_rub|GCF_001468125.1-Lrub_RS10840:0.28400103234765022853,(Le_hac|GCF_000953655.1-LHA_RS10540:0.22347721554597499427,(Le_mas|GCF_000756815.1-BN1094_RS13885:0.13020998569341410422,Ta_mic|GCF_000953635.1-LMI_RS05060:0.22533305279180224079)80:0.07749835096227027098)90:0.09645751839883837109)90:0.15988972268731366233)100:2.15321607527583580577,(((Pe_pis|GCF_000420045.1-F595_RS0105115:1.10677195213708801091,Sp_tro|GCF_000420325.1-G411_RS0113880:1.37916568684941376333)100:2.47420230023553555654,Ph_swi|GCF_001077885.1-AB733_RS16915:4.28961282764486373509)40:0.93296127583722943122,Th_bra|GCF_000828615.1-TBH_RS07150:1.45873177804804421420)20:0.12398441030499342264)10:0.28114274249556370222)10:0.12764452558532890514)10:0.11016681954417295586,((Th_pel|GCF_000711195.1-N746_RS0104105:0.17018233766446777544,Th_aer|GCF_000227665.2-THIAE_RS07035:0.29886979932956742045)100:1.25892221473978405122,(Ha_nea|GCF_000024765.1-HNEAP_RS03580:0.81280359639680321848,Sp_tro|GCF_000420325.1-G411_RS18970:2.91791035882889149278)20:0.19455509611516469404)0:0.08024744668116624846)30:0.17258994118529313866,Ac_pro|GCF_000754095.2-THPRO_RS04390:0.52378958307419987150)10:0.09472909399882875614,((Mi_sed|GCF_000953855.1-MBSD_RS10345:0.59539412013691528358,((St_che|GCF_001431535.1-ABB28_RS08710:0.35979270940335583706,Ly_def|GCF_000423325.1-G545_RS0104900:0.46832353554228262071)90:0.26179569527102258819,(Hy_eff|GCF_000271305.1-WQQ_RS01820:0.50325110573979126016,(Po_alg|GCF_000711245.1-U741_RS0106660:0.52785742722211226319,Al_aro|GCF_000733765.1-U743_RS04095:0.45161089949649030340)60:0.09100702918773677375)50:0.09616622774929543815)80:0.15663175004793034040)60:0.20774374328136244183,(Me_cri|GCF_000421465.1-H035_RS18210:0.73043830982732704804,Me_ory|GCF_001312345.1-JCM16910_RS16915:0.53491473112012599689)60:0.20195251325039759172)0:0.06023714325977676254)10:0.04209689787262102523,Th_sib|GCF_000227725.1-THISIDRAFT_RS00765:0.46293828690839716744)60:0.20336198164939373045,((Ha_gan|GCF_000376785.1-F566_RS0128515:0.24902629697180386770,(Sa_imp|GCF_000423605.1-G565_RS0105565:0.25033807749846487267,Gy_sun|GCF_000940805.1-YC6258_RS19195:0.17785218522211562742)100:0.19875645959206314495)100:0.09537322535965864212,((Ps_aer|GCF_000006765.1-PA1443:0.05738299563023377403,Ps_pro|GCF_000397205.1-PFLCHA0_RS08410:0.14104406393298291045)100:0.19245659724399938328,Ce_jap|GCF_000019225.1-CJA_RS08400:0.38668982191601442011)90:0.14669192550173104639)100:0.29143664626802257001)70:0.44280767161444256619,(Sh_one|GCF_000146165.2-SO_3221:0.09130175697929379663,(Sh_sed|GCF_000018025.1-SSED_RS16030:0.09785419786869108127,Sh_loi|GCF_000016065.1-SHEW_RS07050:0.08926976461563780607)60:0.03694080919466060514)100:0.17358239575740239125)60:0.07954321625934242246,((Mo_das|GCF_000276805.1-A923_RS0107080:0.03314666841259751584,Mo_vis|GCF_000953735.1-MVIS_RS17130:0.02080846294765848620)100:0.29491668858878833381,((Sa_cos|GCF_000565345.1-D481_RS0100625:0.21104162533682152558,(((Vi_cam|GCF_000017705.1-VIBHAR_RS14725:0.08944399287650170449,Vi_met|GCF_000176155.1-VIB_RS04300:0.13433468821398739101)70:0.09516686296320310545,Al_fis|GCF_000011805.1-VF_1845:0.14594727413516661585)70:0.07778081875116071853,Ph_swi|GCF_001077885.1-AB733_RS16615:0.10659843771991510397)60:0.11290467720183175460)70:0.19987403949003224035,(Ae_sim|GCF_000820125.1-BN1134_RS05515:0.05072030716841376130,(Ae_euc|GCF_000819865.1-BN1124_RS12330:0.01996796268593289611,Ae_mol|GCF_000388115.1-G113_RS09235:0.00945931167100205750)90:0.03634711315666208642)100:0.12708711857128088263)20:0.03517382934661902189)30:0.05851529418391160364)70:0.06642405494634517904,Id_bal|GCF_000152885.1-OS145_RS09470:0.16645635126156821393);'
    const complexTreePosY: number[] = [233.6290812942217,20,300.8872438826651,268.79099719929246,240.7895415683962,213.65455483490564,183.1581662735849,149.9719929245283,116.92511792452831,67.8125,38.04245283018868,26.79245283018868,49.29245283018868,36.9811320754717,33.58490566037736,40.37735849056604,61.60377358490566,55.660377358490564,50.56603773584906,47.16981132075472,53.9622641509434,60.75471698113208,67.54716981132076,97.58254716981132,74.33962264150944,120.8254716981132,98.5377358490566,89.62264150943396,84.52830188679245,81.1320754716981,87.9245283018868,94.71698113207547,107.45283018867924,101.50943396226415,113.39622641509433,108.30188679245283,118.49056603773585,115.09433962264151,121.88679245283019,143.1132075471698,137.16981132075472,132.0754716981132,128.67924528301887,135.47169811320754,142.26415094339623,149.0566037735849,166.03773584905662,159.24528301886795,155.84905660377362,162.64150943396228,172.83018867924528,169.43396226415095,176.22641509433961,183.0188679245283,216.34433962264148,198.72641509433961,189.81132075471697,207.64150943396226,200,196.60377358490567,203.39622641509433,215.28301886792454,210.18867924528303,220.37735849056605,216.9811320754717,223.77358490566039,233.96226415094338,230.56603773584905,237.35849056603772,244.15094339622638,267.92452830188677,256.0377358490566,250.94339622641508,261.1320754716981,257.73584905660374,264.52830188679246,279.811320754717,274.7169811320755,271.3207547169811,278.1132075471698,284.90566037735846,296.7924528301887,291.69811320754724,301.88679245283026,298.4905660377359,305.28301886792457,332.98349056603774,315.47169811320754,312.0754716981132,318.8679245283019,350.4952830188679,336.27358490566036,325.66037735849056,346.88679245283015,340.9433962264151,335.84905660377353,332.45283018867923,339.2452830188679,346.0377358490566,352.8301886792453,364.7169811320755,359.62264150943395,369.811320754717,366.4150943396226,373.20754716981133,380]
    const complexTreePosYLad: number[] = [
      353.6183022553066,373.20754716981133,307.64735959610846,265.33009655070754,237.26396668632074,211.6977446933962,186.03699882075472,154.45607311320754,132.6857311320755,106.12617924528303,133.1367924528302,142.26415094339623,124.00943396226415,132.0754716981132,128.67924528301887,135.47169811320754,115.9433962264151,110,104.90566037735849,101.50943396226415,108.30188679245283,115.09433962264151,121.88679245283019,79.11556603773585,94.71698113207547,63.514150943396224,45.04716981132076,55.660377358490564,50.56603773584906,47.16981132075472,53.9622641509434,60.75471698113208,34.43396226415094,40.37735849056604,28.49056603773585,33.58490566037736,23.39622641509434,20,26.79245283018868,81.98113207547169,76.0377358490566,70.9433962264151,67.54716981132076,74.33962264150944,81.1320754716981,87.9245283018868,159.24528301886795,152.45283018867926,149.0566037735849,155.84905660377362,166.03773584905662,162.64150943396228,169.43396226415095,176.22641509433961,217.6179245283019,208.06603773584905,216.9811320754717,199.1509433962264,206.79245283018867,203.39622641509433,210.18867924528303,191.50943396226415,196.60377358490567,186.41509433962264,183.0188679245283,189.81132075471697,227.16981132075472,223.77358490566039,230.56603773584905,237.35849056603772,262.8301886792453,252.64150943396223,257.73584905660374,247.5471698113207,244.15094339622638,250.94339622641508,273.0188679245283,267.92452830188677,264.52830188679246,271.3207547169811,278.1132075471698,293.39622641509436,298.4905660377359,288.3018867924528,284.90566037735846,291.69811320754724,349.96462264150944,363.0188679245283,359.62264150943395,366.4150943396226,336.91037735849056,326.08490566037733,332.45283018867923,319.7169811320755,313.7735849056604,308.6792452830189,305.28301886792457,312.0754716981132,318.8679245283019,325.66037735849056,347.7358490566038,352.8301886792453,342.64150943396226,339.2452830188679,346.0377358490566,380
    ]
    before(async function() {
      this.timeout(15000)
      page = await browser.newPage();
      await page.setViewport(pageSize)
      await page.setContent(`
        <html>
          <head></head>
          <body>
            <h1><center>PhylogicianJS development environment.</center></h1>
            <h3 id="test"></h3>
            <div id='TreeDiv'></div>
            <button id='makeTree' onclick="makeTree()">makeTree</button>
            <button id='ladderize' onclick="ladderize(0)">Ladderize</button>
          </body>
        </html>`)
      await page.addStyleTag({
        content: `
        body {
            margin-top: 30px;
        }
        #TreeDiv {
            width: 800px;
            height: 400px;
            border-width: 1px;
            border-color: black;
            border-style: solid;
            margin:auto;
        }`
      })
      await page.addScriptTag({
        path: './lib/index.browser.js'
      })
      await page.addScriptTag({
        content: `
          const fontSize = '8px'
          const phyloTree = new Phylogician.Phylogician(undefined, '${loglevel}')
          const svgParams = {
            width: 800,
            height: 400
          }
          function makeTree () {
              phyloTree
                .data("${complexTree}")
                .setSvgParameters(svgParams)
                .setLayout('${layout}')
                .setFontSizeAllLabels(fontSize)
                .draw("#TreeDiv")
          }
          let lad = 0
          function ladderize() {
            lad++
            const root = phyloTree.tree.getRootNode()
            document.getElementById("test").innerHTML = 'ladderize(' + root.id + ') - ' + lad
            phyloTree.ladderize(root.id)
          }`
      })
      await page.click('#makeTree')
      await page.waitFor(2000)
      page.on('pageerror', (err: any) => { throw err });
      nodePositionList = await page.evaluate(() => {
        const nodes = document.querySelectorAll('.node')
        const list: any = []
        nodes.forEach((node) => {
          const yStr = node.getAttribute('cy')
          if (yStr) {
            list.push(parseFloat(yStr))
          }
        })
        return list
      })
      await page.click('#ladderize')
      await page.waitFor(1100)
      nodePositionListLadderized = await page.evaluate(() => {
        const nodes = document.querySelectorAll('.node')
        const list: any = []
        nodes.forEach((node) => {
          const yStr = node.getAttribute('cy')
          if (yStr) {
            list.push(parseFloat(yStr))
          }
        })
        return list
      })
    })
    it('tree should be built as is', async () => {
      expect(nodePositionList).not.empty
      nodePositionList.forEach((y: number, i: number) => {
        expect(y - complexTreePosY[i]).to.be.lessThan(tolerance)
      })
    })
    it('Tree should have a different order - known issue', async () => {
      expect(nodePositionListLadderized).not.empty
      nodePositionListLadderized.forEach((y: number, i: number) => {
        expect(y - complexTreePosYLad[i], `Offending position: ${i}`).to.be.lessThan(tolerance)
      })
    })
  })
  describe.skip('Playgroud to play with tooltip', () => {
    let page: any
    const complexTree = '(Rh_per|GCF_000382165.1-H141_RS0111605:0.15110752375429112315,(((((((((Sa_sha|GCF_000215955.2-SSPSH_RS15705:0.87419220310435707955,((Gi_api|GCF_000599985.1-GAPWK_RS12960:0.30672502546257390810,Fr_per|GCF_000807275.1-FPB0191_RS10290:0.30983578673370937295)100:1.10619510105485185925,(((Pl_ger|GCF_000757785.1-LG71_RS13740:0.12671342473269528450,Fr_pul|GCF_000621185.1-Q370_RS0101925:0.09790825799542554608)100:0.12517539486502773927,Xe_dou|GCF_000968195.1-XDD1_RS10665:0.23913905141549529620)100:0.15948416275986720958,Pl_ger|GCF_000757785.1-LG71_RS07410:0.41104394167262958693)90:0.22719977887063710908)100:0.27470042694854696075)100:1.20105555967632593450,(Bd_bac|GCF_000196175.1-BD_RS15235:1.93459186075526656801,((((Le_sao|GCF_001465875.1-BN3124_RS10910:0.28264105214317319925,Fl_dum|GCF_000236165.1-KYQ_RS06510:0.18563006279757618411)90:0.13306084252105129795,Le_pne|GCF_000008485.1-lpg1792:0.37777868894490873597)90:0.21899525628810442845,(Le_rub|GCF_001468125.1-Lrub_RS10840:0.28400103234765022853,(Le_hac|GCF_000953655.1-LHA_RS10540:0.22347721554597499427,(Le_mas|GCF_000756815.1-BN1094_RS13885:0.13020998569341410422,Ta_mic|GCF_000953635.1-LMI_RS05060:0.22533305279180224079)80:0.07749835096227027098)90:0.09645751839883837109)90:0.15988972268731366233)100:2.15321607527583580577,(((Pe_pis|GCF_000420045.1-F595_RS0105115:1.10677195213708801091,Sp_tro|GCF_000420325.1-G411_RS0113880:1.37916568684941376333)100:2.47420230023553555654,Ph_swi|GCF_001077885.1-AB733_RS16915:4.28961282764486373509)40:0.93296127583722943122,Th_bra|GCF_000828615.1-TBH_RS07150:1.45873177804804421420)20:0.12398441030499342264)10:0.28114274249556370222)10:0.12764452558532890514)10:0.11016681954417295586,((Th_pel|GCF_000711195.1-N746_RS0104105:0.17018233766446777544,Th_aer|GCF_000227665.2-THIAE_RS07035:0.29886979932956742045)100:1.25892221473978405122,(Ha_nea|GCF_000024765.1-HNEAP_RS03580:0.81280359639680321848,Sp_tro|GCF_000420325.1-G411_RS18970:2.91791035882889149278)20:0.19455509611516469404)0:0.08024744668116624846)30:0.17258994118529313866,Ac_pro|GCF_000754095.2-THPRO_RS04390:0.52378958307419987150)10:0.09472909399882875614,((Mi_sed|GCF_000953855.1-MBSD_RS10345:0.59539412013691528358,((St_che|GCF_001431535.1-ABB28_RS08710:0.35979270940335583706,Ly_def|GCF_000423325.1-G545_RS0104900:0.46832353554228262071)90:0.26179569527102258819,(Hy_eff|GCF_000271305.1-WQQ_RS01820:0.50325110573979126016,(Po_alg|GCF_000711245.1-U741_RS0106660:0.52785742722211226319,Al_aro|GCF_000733765.1-U743_RS04095:0.45161089949649030340)60:0.09100702918773677375)50:0.09616622774929543815)80:0.15663175004793034040)60:0.20774374328136244183,(Me_cri|GCF_000421465.1-H035_RS18210:0.73043830982732704804,Me_ory|GCF_001312345.1-JCM16910_RS16915:0.53491473112012599689)60:0.20195251325039759172)0:0.06023714325977676254)10:0.04209689787262102523,Th_sib|GCF_000227725.1-THISIDRAFT_RS00765:0.46293828690839716744)60:0.20336198164939373045,((Ha_gan|GCF_000376785.1-F566_RS0128515:0.24902629697180386770,(Sa_imp|GCF_000423605.1-G565_RS0105565:0.25033807749846487267,Gy_sun|GCF_000940805.1-YC6258_RS19195:0.17785218522211562742)100:0.19875645959206314495)100:0.09537322535965864212,((Ps_aer|GCF_000006765.1-PA1443:0.05738299563023377403,Ps_pro|GCF_000397205.1-PFLCHA0_RS08410:0.14104406393298291045)100:0.19245659724399938328,Ce_jap|GCF_000019225.1-CJA_RS08400:0.38668982191601442011)90:0.14669192550173104639)100:0.29143664626802257001)70:0.44280767161444256619,(Sh_one|GCF_000146165.2-SO_3221:0.09130175697929379663,(Sh_sed|GCF_000018025.1-SSED_RS16030:0.09785419786869108127,Sh_loi|GCF_000016065.1-SHEW_RS07050:0.08926976461563780607)60:0.03694080919466060514)100:0.17358239575740239125)60:0.07954321625934242246,((Mo_das|GCF_000276805.1-A923_RS0107080:0.03314666841259751584,Mo_vis|GCF_000953735.1-MVIS_RS17130:0.02080846294765848620)100:0.29491668858878833381,((Sa_cos|GCF_000565345.1-D481_RS0100625:0.21104162533682152558,(((Vi_cam|GCF_000017705.1-VIBHAR_RS14725:0.08944399287650170449,Vi_met|GCF_000176155.1-VIB_RS04300:0.13433468821398739101)70:0.09516686296320310545,Al_fis|GCF_000011805.1-VF_1845:0.14594727413516661585)70:0.07778081875116071853,Ph_swi|GCF_001077885.1-AB733_RS16615:0.10659843771991510397)60:0.11290467720183175460)70:0.19987403949003224035,(Ae_sim|GCF_000820125.1-BN1134_RS05515:0.05072030716841376130,(Ae_euc|GCF_000819865.1-BN1124_RS12330:0.01996796268593289611,Ae_mol|GCF_000388115.1-G113_RS09235:0.00945931167100205750)90:0.03634711315666208642)100:0.12708711857128088263)20:0.03517382934661902189)30:0.05851529418391160364)70:0.06642405494634517904,Id_bal|GCF_000152885.1-OS145_RS09470:0.16645635126156821393);'
    before(async function() {
      this.timeout(15000)
      page = await browser.newPage();
      await page.setViewport(pageSize)
      await page.setContent(`
        <html>
          <head></head>
          <body>
            <h1><center>PhylogicianJS development environment.</center></h1>
            <h3 id="test"></h3>
            <div id='TreeDiv'></div>
            <button id='makeTree' onclick="makeTree()">makeTree</button>
            <button id='ladderize' onclick="ladderize(0)">Ladderize</button>
          </body>
        </html>`)
      await page.addStyleTag({
        content: `
        body {
            margin-top: 30px;
        }
        #TreeDiv {
            width: 800px;
            height: 2000px;
            border-width: 1px;
            border-color: black;
            border-style: solid;
            margin:auto;
        }`
      })
      await page.addScriptTag({
        path: './lib/index.browser.js'
      })
      await page.addScriptTag({
        content: `
          const fontSize = '13px'
          const phyloTree = new Phylogician.Phylogician(undefined, '${loglevel}')
          const svgParams = {
            width: 800,
            height: 2000
          }
          function makeTree () {
              phyloTree
                .data("${complexTree}")
                .setSvgParameters(svgParams)
                .setLayout('${layout}')
                .setFontSizeAllLabels(fontSize)
                .draw("#TreeDiv")
          }
          `
      })
      await page.click('#makeTree')
      await page.waitFor(2000)
    })
    it('should do something', async () => {
      expect(1, 'playground').eql(0)
    })
  })
  describe('Phylogician should not interfere with other svg containers', () => {
    describe('VerticalTreeLayout', () => {
      let page: any
      const originalAttrOtherSvg = {
        h: 30,
        w: 20
      }
      let attrOtherSvg: any
      before(async function() {
        this.timeout(15000)
        page = await browser.newPage();
        await page.setViewport(pageSize)
        await page.setContent(`
          <html>
            <head></head>
            <body>
              <h1><center>PhylogicianJS development environment.</center></h1>
              <h2><center>Testing VerticalTreeLayout not to interfere with other svg elements</center></h2>
              <svg width="${originalAttrOtherSvg.w}px" height="${originalAttrOtherSvg.h}px" id="otherSvg">
                <rect width="${originalAttrOtherSvg.w}px" height="${originalAttrOtherSvg.h}px" fill="red"/>
              </svg>
              <div id='TreeDiv'></div>
              <button id='scale' onclick="clicked()">scale</button>
              <button id='makeTree' onclick="makeTree()">makeTree</button>
            </body>
          </html>`)
        await page.addStyleTag({
          content: `
          body {
              margin-top: 30px;
          }
          #TreeDiv {
              width: 800px;
              height: 500px;
              border-width: 1px;
              border-color: black;
              border-style: solid;
              margin:auto;
          }`
        })
        await page.addScriptTag({
          path: './lib/index.browser.js'
        })
        await page.addScriptTag({
          content: `
            const phyloTree = new Phylogician.Phylogician(undefined, '${loglevel}')
            const svgParam = {
              width: 100
            }
            const newSvgParams = {
              width: 300
            }
            function makeTree () {
                phyloTree
                  .data("${newick}")
                  .setSvgParameters(svgParam)
                  .setLayout('${layout}')
                  .draw("#TreeDiv")
            }
            function clicked () {
                phyloTree.setSvgParameters(newSvgParams)
            }`
        })
        await page.click('#makeTree')
        await page.waitFor(2000)
        await page.click('#scale')
        await page.waitFor(1100)
        attrOtherSvg = await page.evaluate(() => {
          const svg = document.querySelector('#otherSvg')
          let attr: {w: number, h: number}
          if (svg) {
            const width = svg.getAttribute('width')
            const height = svg.getAttribute('height')
            if (width && height) {
              attr = {
                h: parseFloat(height),
                w: parseFloat(width)
              }
              return attr
            }
          }
        })
      })
      it('all nodes should line up with this tree with scaling (no branch lengths)', async () => {
        expect(originalAttrOtherSvg).eql(attrOtherSvg)
      })
    })
  })  
})