// tslint:disable: no-unused-expression
import { expect } from 'chai'
import fs from 'fs'
import performance from 'performance-now'
import { Tree } from '../src/Tree'

const levelLog = 'silent'

const largeTree = fs.readFileSync('data/bac.tree').toString()

describe('Tree', () => {
    describe('buildTree', () => {
        describe('should throw when', () => {
            const nwk = '(A:2,(B:3,C:4):5);'
            const tree = new Tree()
            tree.buildTree(nwk)
            it('buildTree is called more than once in the same instance', () => {
                expect(() => tree.buildTree(nwk)).to.throw('There is a phylogeny in this instance with 3 leafs')
            })
            it('tree array must have the correct branch lengths', () => {
                const branchLengthArray = tree.nodes.map((node) => {
                    return node.branchLength
                })
                const expected = [null, 2, 5, 3, 4]
                expect(branchLengthArray).eql(expected)
            })
            it('should set the right parents', () => {
                const parentNodeIds = tree.nodes.map((node) => node.getParentNodeId())
                const expected = [null, 0, 0, 2, 2]
                expect(parentNodeIds).eql(expected)
            })
        })
        describe('with simple newick', () => {
            const nwk = '(A:2,(B:3,C:4):5);'
            const tree = new Tree()
            tree.buildTree(nwk)
            it('tree array must have node ids in order', () => {
                const idArray = tree.nodes.map((node) => {
                    return node.id
                })
                const expected = [0, 1, 2, 3, 4]
                expect(idArray).eql(expected)
            })
            it('tree array must have the correct branch lengths', () => {
                const branchLengthArray = tree.nodes.map((node) => {
                    return node.branchLength
                })
                const expected = [null, 2, 5, 3, 4]
                expect(branchLengthArray).eql(expected)
            })
            it('should set the right parents', () => {
                const parentNodeIds = tree.nodes.map((node) => node.getParentNodeId())
                const expected = [null, 0, 0, 2, 2]
                expect(parentNodeIds).eql(expected)
            })
        })
        describe('with complex newick', () => {
            const nwk = '((Alice:2,Debbie:3):4,((Bob:3,Charlie:4):2,Ellen:5):3);'
            const tree = new Tree()
            tree.buildTree(nwk)
            it('tree array must have node ids in order', () => {
                const idArray = tree.nodes.map((node) => {
                    return node.id
                })
                const expected = [0, 1, 2, 3, 4, 5, 6, 7, 8]
                expect(idArray).eql(expected)
            })
            it('tree array must have the correct branch lengths', () => {
                const branchLengthArray = tree.nodes.map((node) => {
                    return node.branchLength
                })
                const expected = [null, 4, 2, 3, 3, 2, 3, 4, 5]
                expect(branchLengthArray).eql(expected)
            })
            it('should set the right parents', () => {
                const parentNodeIds = tree.nodes.map((node) => node.getParentNodeId())
                const expected = [null, 0, 1, 1, 0, 4, 5, 5, 4]
                expect(parentNodeIds).eql(expected)
            })
        })
    })
    describe('getNumberOfLeafs', () => {
        it('should count leaves', () => {
            const nwk = '(A:2,(B:3,C:4):5);'
            const expected = 3
            const tree = new Tree()
            tree.buildTree(nwk)
            const nLeaves = tree.getNumberOfLeafs()
            expect(nLeaves).eql(expected)
        })
    })
    describe('findNodeByExactName', () => {
        it('Must find the right node by name', () => {
            const nwk = '(A:2,(B:3,C:4):5);'
            const selectNodeName = 'B'
            const tree = new Tree()
            tree.buildTree(nwk)
            const selectedNode = tree.findNodeByExactName(selectNodeName)
            expect(selectedNode.length).eql(1)
            const node = selectedNode[0]
            expect(node.name).eql(selectNodeName)
            expect(node.id).eql(3)
        })
    })
    describe('findNodeByMatch', () => {
        it('Must find the right node by name', () => {
            const nwk = '(A:2,(B34Krep4:3,C:4):5);'
            const re = new RegExp(/rep/)
            const expectedNodeName = 'B34Krep4'
            const tree = new Tree()
            tree.buildTree(nwk)
            const selectedNode = tree.findNodeByMatch(re)
            expect(selectedNode.length).eql(1)
            const node = selectedNode[0]
            expect(node.name).eql(expectedNodeName)
            expect(node.id).eql(3)
        })
    })
    describe('getNode', () => {
        it('should get the correct node', () => {
            const nwk = '(A:2,(B:3,C:4):5);'
            const selectNodeId = 3
            const tree = new Tree()
            tree.buildTree(nwk)
            const selectedNode = tree.getNode(selectNodeId)
            expect(selectedNode.id).eql(selectNodeId)
            expect(selectedNode.name).eql('B')
        })
        it('should throw if there is no node with that Id', () => {
            const nwk = '(A:2,(B:3,C:4):5);'
            const selectNodeId = 10
            const tree = new Tree()
            tree.buildTree(nwk)
            expect(() => {
                tree.getNode(selectNodeId)
            }).to.throw(`Id 10 not found`)
        })
    })
    describe('getMaxDistanceToRoot', () => {
        it('should get the correct max distance to root - sum of branch lengths', () => {
            const nwk = '(A:2,(B:3,C:4):5);'
            const expectedMaxDistanceToRoot = 9
            const tree = new Tree()
            tree.buildTree(nwk)
            const maxDistanceToRoot = tree.getMaxDistanceToRoot()
            expect(maxDistanceToRoot).eql(expectedMaxDistanceToRoot)
        })
        it('should get the correct max distance to root - sum of branch lengths - with complex tree', () => {
            const nwk = '((A2lice:2,D3ebbie:3)1N:4,((B6ob:3,C7harlie:4,((E10llen:3,F11elix:4)N9:2,G12il:5)N8:3)N5:2,I13lly:5)N4:3)0N;'
            const expectedMaxDistanceToRoot = 4 + 2 + 3 + 2 + 3
            const tree = new Tree()
            tree.buildTree(nwk)
            const maxDistanceToRoot = tree.getMaxDistanceToRoot()
            expect(maxDistanceToRoot).eql(expectedMaxDistanceToRoot)
        })
    })
    describe('getMaxHopsToRoot', () => {
        it('should get the correct max number of hops (internal nodes) to root', () => {
            const nwk = '((Alice:2,Debbie:3):4,((Bob:3,Charlie:4):2,Ellen:5):3);'
            const expectedMaxHopsToRoot = 3
            const tree = new Tree()
            tree.buildTree(nwk)
            const maxHopsToRoot = tree.getMaxHopsToRoot()
            expect(maxHopsToRoot).eql(expectedMaxHopsToRoot)
        })
    })
    describe('getPathToRoot', () => {
        it('should get the correct max number of hops (internal nodes) to root', () => {
            const nwk = '((A2lice:2,D3ebbie:3)1N:4,((B6ob:3,C7harlie:4,((E10llen:3,F11elix:4)N9:2,G12il:5)N8:3)N5:2,I13lly:5)N4:3)0N;'
            const expectedPathToRoot = [10,9,8,5,4,0]
            const tree = new Tree()
            tree.buildTree(nwk)
            const path = tree.getPathToRoot(10)
            expect(path).eql(expectedPathToRoot)
        })
    })
    describe('getAllMaxHopsToChildLeafs', () => {
        it('should get the correct max number of hops (internal nodes) to root', () => {
            const nwk = '((Alice:2,Debbie:3):4,((Bob:3,Charlie:4):2,Ellen:5):3);'
            const expected = [3, 1, 0, 0, 2, 1, 0, 0, 0]
            const tree = new Tree()
            tree.buildTree(nwk)
            const maxHopsToRoot = tree.getAllMaxHopsToChildLeafs()
            expect(maxHopsToRoot).eql(expected)
        })
        it('should get the correct max number of hops (internal nodes) to root with complicated tree', () => {
            const nwk = '((A2lice:2,D3ebbie:3)1N:4,((B6ob:3,C7harlie:4,((E10llen:3,F11elix:4)N9:2,G12il:5)N8:3)N5:2,I13lly:5)N4:3)0N;'
            const expected = [5, 1, 0, 0, 4, 3, 0, 0, 2, 1, 0, 0, 0, 0]
            const tree = new Tree()
            tree.buildTree(nwk)
            const maxHopsToRoot = tree.getAllMaxHopsToChildLeafs()
            expect(maxHopsToRoot).eql(expected)
        })
    })
    describe('getCommonAncestor', () => {
        it('should work with easy case', () => {
            const tree = new Tree(levelLog)
            const nwk = '((A2lice:2,D3ebbie:3)1N:4,((B6ob:3,C7harlie:4,((E10llen:3,F11elix:4)N9:2,G12il:5)N8:3)N5:2,I13lly:5)N4:3)0N;'
            tree.buildTree(nwk)
            const node1 = 2
            const node2 = 3
            const ac = tree.getCommonAncestor(node1, node2)
            console.log(ac)
            const expectedCommonAncestor = 1
            expect(ac.ancestor).eql(expectedCommonAncestor)
            const expectedNumberOfHops = 1
            expect(ac.hops).eql(expectedNumberOfHops)
        })
        it('should work with harder case', () => {
            const tree = new Tree(levelLog)
            const nwk = '((A2lice:2,D3ebbie:3)1N:4,((B6ob:3,C7harlie:4,((E10llen:3,F11elix:4)N9:2,G12il:5)N8:3)N5:2,I13lly:5)N4:3)0N;'
            tree.buildTree(nwk)
            const node1 = 10
            const node2 = 13
            const ac = tree.getCommonAncestor(node1, node2)
            console.log(ac)
            const expectedCommonAncestor = 4
            expect(ac.ancestor).eql(expectedCommonAncestor)
            const expectedNumberOfHops = 4
            expect(ac.hops).eql(expectedNumberOfHops)
        })
    })
    describe('getCommonAncestorWithEachLeaf', () => {
        it('should work with easy case', () => {
            const tree = new Tree(levelLog)
            const nwk = '((A2lice:2,D3ebbie:3)1N:4,((B6ob:3,C7harlie:4,((E10llen:3,F11elix:4)N9:2,G12il:5)N8:3)N5:2,I13lly:5)N4:3)0N;'
            tree.buildTree(nwk)
            const node = 2
            const acs = tree.getCommonAncestorWithEachLeaf(node, 0)
            const expected = [
                { ancestor: 1, hops: 1, nodeId: 3 },
                { ancestor: 0, hops: 2, nodeId: 6 },
                { ancestor: 0, hops: 2, nodeId: 7 },
                { ancestor: 0, hops: 2, nodeId: 10 },
                { ancestor: 0, hops: 2, nodeId: 11 },
                { ancestor: 0, hops: 2, nodeId: 12 },
                { ancestor: 0, hops: 2, nodeId: 13 },
            ]
            expect(acs).eql(expected)
        })
        it('should work with harder case', () => {
            const tree = new Tree(levelLog)
            const nwk = '((A2lice:2,D3ebbie:3)1N:4,((B6ob:3,C7harlie:4,((E10llen:3,F11elix:4)N9:2,G12il:5)N8:3)N5:2,I13lly:5)N4:3)0N;'
            tree.buildTree(nwk)
            const node = 10
            const acs = tree.getCommonAncestorWithEachLeaf(node, 0)
            const expected = [
                { ancestor: 9, hops: 1, nodeId: 11 },
                { ancestor: 8, hops: 2, nodeId: 12 },
                { ancestor: 5, hops: 3, nodeId: 6 },
                { ancestor: 5, hops: 3, nodeId: 7 },
                { ancestor: 4, hops: 4, nodeId: 13 },
                { ancestor: 0, hops: 5, nodeId: 2 },
                { ancestor: 0, hops: 5, nodeId: 3 },
              ]
            expect(acs).eql(expected)
        })
        it('should work with subtree', () => {
            const tree = new Tree(levelLog)
            const nwk = '((A2lice:2,D3ebbie:3)1N:4,((B6ob:3,C7harlie:4,((E10llen:3,F11elix:4)N9:2,G12il:5)N8:3)N5:2,I13lly:5)N4:3)0N;'
            tree.buildTree(nwk)
            const node = 10
            const acs = tree.getCommonAncestorWithEachLeaf(node, 5)
            const expected = [
                { ancestor: 9, hops: 1, nodeId: 11 },
                { ancestor: 8, hops: 2, nodeId: 12 },
                { ancestor: 5, hops: 3, nodeId: 6 },
                { ancestor: 5, hops: 3, nodeId: 7 }
            ]
            expect(acs).eql(expected)
        })
    })
    describe('getRootNode', () => {
        it('should grab the (correct) root node', () => {
            const nwk = '((A2lice:2,D3ebbie:3)1N:4,((B6ob:3,C7harlie:4,((E10llen:3,F11elix:4)N9:2,G12il:5)N8:3)N5:2,I13lly:5)N4:3)0N;'
            const expectedRootName = '0N'
            const tree = new Tree()
            tree.buildTree(nwk)
            const rootNode = tree.getRootNode()
            expect(rootNode).not.to.be.null
            if (rootNode) {
                expect(rootNode.name).eql(expectedRootName)
            }
        })
        it('should throw if more than 1 root is assigned', () => {
            const nwk = '((A2lice:2,D3ebbie:3)1N:4,((B6ob:3,C7harlie:4,((E10llen:3,F11elix:4)N9:2,G12il:5)N8:3)N5:2,I13lly:5)N4:3)0N;'
            const tree = new Tree()
            tree.buildTree(nwk)
            tree.nodes[1].setAsRoot()
            expect(() => tree.getRootNode()).to.throw('There are 2 roots with ids [0, 1] assigned.')
        })
    })
    describe('getInternalIds', () => {
        it('should work', () => {
            const nwk = '((A2lice:2,D3ebbie:3)1N:4,((B6ob:3,C7harlie:4,((E10llen:3,F11elix:4)N9:2,G12il:5)N8:3)N5:2,I13lly:5)N4:3)0N;'
            const expectedInternalNodesIds = [0, 1, 4, 5, 8, 9]
            const tree = new Tree()
            tree.buildTree(nwk)
            const internalIds = tree.getInternalIds()
            expect(internalIds).eql(expectedInternalNodesIds)
        })
    })
    describe('setAsRoot', () => {
        it('should let you pick a different root', () => {
            const nwk = '((A2lice:2,D3ebbie:3)1N:4,((B6ob:3,C7harlie:4,((E10llen:3,F11elix:4)N9:2,G12il:5)N8:3)N5:2,I13lly:5)N4:3)0N;'
            const tree = new Tree()
            tree.buildTree(nwk)
            const currentRoot = tree.getRootNode()
            expect(currentRoot).not.to.be.null
            if (currentRoot) {
                expect(currentRoot.id).eql(0)
            }
            tree.setAsRoot(1)
            const newRoot = tree.getRootNode()
            expect(newRoot).not.to.be.null
            if (newRoot) {
                expect(newRoot.id).eql(1)
            }
            const oldRoot = tree.nodes[0]
            expect(oldRoot.isRoot()).to.be.false
        })
    })
    describe('getChildrenIds', () => {
        it('should get the right children', () => {
            const nwk = '((A2lice:2,D3ebbie:3)1N:4,((B6ob:3,C7harlie:4,((E10llen:3,F11elix:4)N9:2,G12il:5)N8:3)N5:2,I13lly:5)N4:3)0N;'
            const expectedChildren = [6, 7, 8, 9, 10, 11, 12]
            const tree = new Tree()
            tree.buildTree(nwk)
            const start = performance()
            const internalIds = tree.getChildrenIds(5)
            console.log(`time lapsed: ${performance() - start}`)
            expect(internalIds).eql(expectedChildren)
        })
        it.skip('should get the children quickly - large tree', () => {
            const expectedChildrenNum = 46914
            for (let i = 2; i > 0 ; i--) {
                const tree = new Tree()
                tree.buildTree(largeTree)
                const start = performance()
                const internalIds = tree.getChildrenIds(0)
                console.log(`${i} - time lapsed: ${performance() - start}`)    
            }
            // expect(internalIds.length).eql(expectedChildrenNum)
        })
    })
    describe('getLeafsIds', () => {
        it('should get the right leafs', () => {
            const nwk = '((A2lice:2,D3ebbie:3)1N:4,((B6ob:3,C7harlie:4,((E10llen:3,F11elix:4)N9:2,G12il:5)N8:3)N5:2,I13lly:5)N4:3)0N;'
            const expectedLeafs = [6, 7, 10, 11, 12]
            const tree = new Tree()
            tree.buildTree(nwk)
            const internalIds = tree.getLeafsIds(5)
            expect(internalIds).eql(expectedLeafs)
        })
    })
    describe('ladderize', () => {
        it('should order ladderize the nodes', () => {
            const nwk = '((A2lice:2,D3ebbie:3)1N:4,((B6ob:3,C7harlie:4,((E10llen:3,F11elix:4)N9:2,G12il:5)N8:3)N5:2,I13lly:5)N4:3)0N;'
            const expectedNodeOrder = [ 0, 4, 5, 8, 9, 10, 11, 12, 6, 7, 13, 1, 2, 3 ]
            const tree = new Tree()
            tree.buildTree(nwk).ladderize(0)
            const nodeIds = tree.getOrder()
            expect(nodeIds).eql(expectedNodeOrder)
        })
        it('should order ladderize the nodes of this tree too', () => {
            const nwk = '(((C3N,(A4N,B5N)2N)1N,((F8N,(D10N,E11N)9N)7N,(I13N,(G15N,H16N)14N)12N)6N);'
            const expectedNodeOrder = [ 10, 11, 8, 15, 16, 13, 4, 5, 2 ]
            const tree = new Tree()
            tree.buildTree(nwk).ladderize(0)
            const nodeLeafIds = tree.getLeafsIds(0)
            expect(nodeLeafIds).eql(expectedNodeOrder)
        })
        it('should order ladderize internal nodes', () => {
            const nwk = '((A2lice:2,D3ebbie:3)1N:4,((B6ob:3,C7harlie:4,((E10llen:3,F11elix:4)N9:2,G12il:5)N8:3)N5:2,I13lly:5)N4:3)0N;'
            const expectedNodeOrder = [ 0, 1, 2, 3, 4, 5, 8, 9, 10, 11, 12, 6, 7, 13 ]
            const tree = new Tree('debug')
            tree.buildTree(nwk).ladderize(5)
            const nodeIds = tree.getOrder()
            // console.log(nodeIds)
            expect(nodeIds).eql(expectedNodeOrder)
        })
        it.skip('should work with complex trees', () => {
            const complexTree = '(Rh_per|GCF_000382165.1-H141_RS0111605:0.15110752375429112315,(((((((((Sa_sha|GCF_000215955.2-SSPSH_RS15705:0.87419220310435707955,((Gi_api|GCF_000599985.1-GAPWK_RS12960:0.30672502546257390810,Fr_per|GCF_000807275.1-FPB0191_RS10290:0.30983578673370937295)100:1.10619510105485185925,(((Pl_ger|GCF_000757785.1-LG71_RS13740:0.12671342473269528450,Fr_pul|GCF_000621185.1-Q370_RS0101925:0.09790825799542554608)100:0.12517539486502773927,Xe_dou|GCF_000968195.1-XDD1_RS10665:0.23913905141549529620)100:0.15948416275986720958,Pl_ger|GCF_000757785.1-LG71_RS07410:0.41104394167262958693)90:0.22719977887063710908)100:0.27470042694854696075)100:1.20105555967632593450,(Bd_bac|GCF_000196175.1-BD_RS15235:1.93459186075526656801,((((Le_sao|GCF_001465875.1-BN3124_RS10910:0.28264105214317319925,Fl_dum|GCF_000236165.1-KYQ_RS06510:0.18563006279757618411)90:0.13306084252105129795,Le_pne|GCF_000008485.1-lpg1792:0.37777868894490873597)90:0.21899525628810442845,(Le_rub|GCF_001468125.1-Lrub_RS10840:0.28400103234765022853,(Le_hac|GCF_000953655.1-LHA_RS10540:0.22347721554597499427,(Le_mas|GCF_000756815.1-BN1094_RS13885:0.13020998569341410422,Ta_mic|GCF_000953635.1-LMI_RS05060:0.22533305279180224079)80:0.07749835096227027098)90:0.09645751839883837109)90:0.15988972268731366233)100:2.15321607527583580577,(((Pe_pis|GCF_000420045.1-F595_RS0105115:1.10677195213708801091,Sp_tro|GCF_000420325.1-G411_RS0113880:1.37916568684941376333)100:2.47420230023553555654,Ph_swi|GCF_001077885.1-AB733_RS16915:4.28961282764486373509)40:0.93296127583722943122,Th_bra|GCF_000828615.1-TBH_RS07150:1.45873177804804421420)20:0.12398441030499342264)10:0.28114274249556370222)10:0.12764452558532890514)10:0.11016681954417295586,((Th_pel|GCF_000711195.1-N746_RS0104105:0.17018233766446777544,Th_aer|GCF_000227665.2-THIAE_RS07035:0.29886979932956742045)100:1.25892221473978405122,(Ha_nea|GCF_000024765.1-HNEAP_RS03580:0.81280359639680321848,Sp_tro|GCF_000420325.1-G411_RS18970:2.91791035882889149278)20:0.19455509611516469404)0:0.08024744668116624846)30:0.17258994118529313866,Ac_pro|GCF_000754095.2-THPRO_RS04390:0.52378958307419987150)10:0.09472909399882875614,((Mi_sed|GCF_000953855.1-MBSD_RS10345:0.59539412013691528358,((St_che|GCF_001431535.1-ABB28_RS08710:0.35979270940335583706,Ly_def|GCF_000423325.1-G545_RS0104900:0.46832353554228262071)90:0.26179569527102258819,(Hy_eff|GCF_000271305.1-WQQ_RS01820:0.50325110573979126016,(Po_alg|GCF_000711245.1-U741_RS0106660:0.52785742722211226319,Al_aro|GCF_000733765.1-U743_RS04095:0.45161089949649030340)60:0.09100702918773677375)50:0.09616622774929543815)80:0.15663175004793034040)60:0.20774374328136244183,(Me_cri|GCF_000421465.1-H035_RS18210:0.73043830982732704804,Me_ory|GCF_001312345.1-JCM16910_RS16915:0.53491473112012599689)60:0.20195251325039759172)0:0.06023714325977676254)10:0.04209689787262102523,Th_sib|GCF_000227725.1-THISIDRAFT_RS00765:0.46293828690839716744)60:0.20336198164939373045,((Ha_gan|GCF_000376785.1-F566_RS0128515:0.24902629697180386770,(Sa_imp|GCF_000423605.1-G565_RS0105565:0.25033807749846487267,Gy_sun|GCF_000940805.1-YC6258_RS19195:0.17785218522211562742)100:0.19875645959206314495)100:0.09537322535965864212,((Ps_aer|GCF_000006765.1-PA1443:0.05738299563023377403,Ps_pro|GCF_000397205.1-PFLCHA0_RS08410:0.14104406393298291045)100:0.19245659724399938328,Ce_jap|GCF_000019225.1-CJA_RS08400:0.38668982191601442011)90:0.14669192550173104639)100:0.29143664626802257001)70:0.44280767161444256619,(Sh_one|GCF_000146165.2-SO_3221:0.09130175697929379663,(Sh_sed|GCF_000018025.1-SSED_RS16030:0.09785419786869108127,Sh_loi|GCF_000016065.1-SHEW_RS07050:0.08926976461563780607)60:0.03694080919466060514)100:0.17358239575740239125)60:0.07954321625934242246,((Mo_das|GCF_000276805.1-A923_RS0107080:0.03314666841259751584,Mo_vis|GCF_000953735.1-MVIS_RS17130:0.02080846294765848620)100:0.29491668858878833381,((Sa_cos|GCF_000565345.1-D481_RS0100625:0.21104162533682152558,(((Vi_cam|GCF_000017705.1-VIBHAR_RS14725:0.08944399287650170449,Vi_met|GCF_000176155.1-VIB_RS04300:0.13433468821398739101)70:0.09516686296320310545,Al_fis|GCF_000011805.1-VF_1845:0.14594727413516661585)70:0.07778081875116071853,Ph_swi|GCF_001077885.1-AB733_RS16615:0.10659843771991510397)60:0.11290467720183175460)70:0.19987403949003224035,(Ae_sim|GCF_000820125.1-BN1134_RS05515:0.05072030716841376130,(Ae_euc|GCF_000819865.1-BN1124_RS12330:0.01996796268593289611,Ae_mol|GCF_000388115.1-G113_RS09235:0.00945931167100205750)90:0.03634711315666208642)100:0.12708711857128088263)20:0.03517382934661902189)30:0.05851529418391160364)70:0.06642405494634517904,Id_bal|GCF_000152885.1-OS145_RS09470:0.16645635126156821393);'
            const initialOrder = [
                'Rh_per|GCF_000382165.1-H141_RS0111605',
                'Sa_sha|GCF_000215955.2-SSPSH_RS15705',
                'Gi_api|GCF_000599985.1-GAPWK_RS12960',
                'Fr_per|GCF_000807275.1-FPB0191_RS10290',
                'Pl_ger|GCF_000757785.1-LG71_RS13740',
                'Fr_pul|GCF_000621185.1-Q370_RS0101925',
                'Xe_dou|GCF_000968195.1-XDD1_RS10665',
                'Pl_ger|GCF_000757785.1-LG71_RS07410',
                'Bd_bac|GCF_000196175.1-BD_RS15235',
                'Le_sao|GCF_001465875.1-BN3124_RS10910',
                'Fl_dum|GCF_000236165.1-KYQ_RS06510',
                'Le_pne|GCF_000008485.1-lpg1792',
                'Le_rub|GCF_001468125.1-Lrub_RS10840',
                'Le_hac|GCF_000953655.1-LHA_RS10540',
                'Le_mas|GCF_000756815.1-BN1094_RS13885',
                'Ta_mic|GCF_000953635.1-LMI_RS05060',
                'Pe_pis|GCF_000420045.1-F595_RS0105115',
                'Sp_tro|GCF_000420325.1-G411_RS0113880',
                'Ph_swi|GCF_001077885.1-AB733_RS16915',
                'Th_bra|GCF_000828615.1-TBH_RS07150',
                'Th_pel|GCF_000711195.1-N746_RS0104105',
                'Th_aer|GCF_000227665.2-THIAE_RS07035',
                'Ha_nea|GCF_000024765.1-HNEAP_RS03580',
                'Sp_tro|GCF_000420325.1-G411_RS18970',
                'Ac_pro|GCF_000754095.2-THPRO_RS04390',
                'Mi_sed|GCF_000953855.1-MBSD_RS10345',
                'St_che|GCF_001431535.1-ABB28_RS08710',
                'Ly_def|GCF_000423325.1-G545_RS0104900',
                'Hy_eff|GCF_000271305.1-WQQ_RS01820',
                'Po_alg|GCF_000711245.1-U741_RS0106660',
                'Al_aro|GCF_000733765.1-U743_RS04095',
                'Me_cri|GCF_000421465.1-H035_RS18210',
                'Me_ory|GCF_001312345.1-JCM16910_RS16915',
                'Th_sib|GCF_000227725.1-THISIDRAFT_RS00765',
                'Ha_gan|GCF_000376785.1-F566_RS0128515',
                'Sa_imp|GCF_000423605.1-G565_RS0105565',
                'Gy_sun|GCF_000940805.1-YC6258_RS19195',
                'Ps_aer|GCF_000006765.1-PA1443',
                'Ps_pro|GCF_000397205.1-PFLCHA0_RS08410',
                'Ce_jap|GCF_000019225.1-CJA_RS08400',
                'Sh_one|GCF_000146165.2-SO_3221',
                'Sh_sed|GCF_000018025.1-SSED_RS16030',
                'Sh_loi|GCF_000016065.1-SHEW_RS07050',
                'Mo_das|GCF_000276805.1-A923_RS0107080',
                'Mo_vis|GCF_000953735.1-MVIS_RS17130',
                'Sa_cos|GCF_000565345.1-D481_RS0100625',
                'Vi_cam|GCF_000017705.1-VIBHAR_RS14725',
                'Vi_met|GCF_000176155.1-VIB_RS04300',
                'Al_fis|GCF_000011805.1-VF_1845',
                'Ph_swi|GCF_001077885.1-AB733_RS16615',
                'Ae_sim|GCF_000820125.1-BN1134_RS05515',
                'Ae_euc|GCF_000819865.1-BN1124_RS12330',
                'Ae_mol|GCF_000388115.1-G113_RS09235',
                'Id_bal|GCF_000152885.1-OS145_RS09470'
              ]
            const expectedFinalOrder = [
                'Rh_per|GCF_000382165.1-H141_RS0111605',
                'Sa_sha|GCF_000215955.2-SSPSH_RS15705',
                'Gi_api|GCF_000599985.1-GAPWK_RS12960',
                'Fr_per|GCF_000807275.1-FPB0191_RS10290',
                'Pl_ger|GCF_000757785.1-LG71_RS13740',
                'Fr_pul|GCF_000621185.1-Q370_RS0101925',
                'Xe_dou|GCF_000968195.1-XDD1_RS10665',
                'Pl_ger|GCF_000757785.1-LG71_RS07410',
                'Bd_bac|GCF_000196175.1-BD_RS15235',
                'Le_sao|GCF_001465875.1-BN3124_RS10910',
                'Fl_dum|GCF_000236165.1-KYQ_RS06510',
                'Le_pne|GCF_000008485.1-lpg1792',
                'Le_rub|GCF_001468125.1-Lrub_RS10840',
                'Le_hac|GCF_000953655.1-LHA_RS10540',
                'Le_mas|GCF_000756815.1-BN1094_RS13885',
                'Ta_mic|GCF_000953635.1-LMI_RS05060',
                'Pe_pis|GCF_000420045.1-F595_RS0105115',
                'Sp_tro|GCF_000420325.1-G411_RS0113880',
                'Ph_swi|GCF_001077885.1-AB733_RS16915',
                'Th_bra|GCF_000828615.1-TBH_RS07150',
                'Th_pel|GCF_000711195.1-N746_RS0104105',
                'Th_aer|GCF_000227665.2-THIAE_RS07035',
                'Ha_nea|GCF_000024765.1-HNEAP_RS03580',
                'Sp_tro|GCF_000420325.1-G411_RS18970',
                'Ac_pro|GCF_000754095.2-THPRO_RS04390',
                'Mi_sed|GCF_000953855.1-MBSD_RS10345',
                'St_che|GCF_001431535.1-ABB28_RS08710',
                'Ly_def|GCF_000423325.1-G545_RS0104900',
                'Hy_eff|GCF_000271305.1-WQQ_RS01820',
                'Po_alg|GCF_000711245.1-U741_RS0106660',
                'Al_aro|GCF_000733765.1-U743_RS04095',
                'Me_cri|GCF_000421465.1-H035_RS18210',
                'Me_ory|GCF_001312345.1-JCM16910_RS16915',
                'Th_sib|GCF_000227725.1-THISIDRAFT_RS00765',
                'Ha_gan|GCF_000376785.1-F566_RS0128515',
                'Sa_imp|GCF_000423605.1-G565_RS0105565',
                'Gy_sun|GCF_000940805.1-YC6258_RS19195',
                'Ps_aer|GCF_000006765.1-PA1443',
                'Ps_pro|GCF_000397205.1-PFLCHA0_RS08410',
                'Ce_jap|GCF_000019225.1-CJA_RS08400',
                'Sh_one|GCF_000146165.2-SO_3221',
                'Sh_sed|GCF_000018025.1-SSED_RS16030',
                'Sh_loi|GCF_000016065.1-SHEW_RS07050',
                'Mo_das|GCF_000276805.1-A923_RS0107080',
                'Mo_vis|GCF_000953735.1-MVIS_RS17130',
                'Sa_cos|GCF_000565345.1-D481_RS0100625',
                'Vi_cam|GCF_000017705.1-VIBHAR_RS14725',
                'Vi_met|GCF_000176155.1-VIB_RS04300',
                'Al_fis|GCF_000011805.1-VF_1845',
                'Ph_swi|GCF_001077885.1-AB733_RS16615',
                'Ae_sim|GCF_000820125.1-BN1134_RS05515',
                'Ae_euc|GCF_000819865.1-BN1124_RS12330',
                'Ae_mol|GCF_000388115.1-G113_RS09235',
                'Id_bal|GCF_000152885.1-OS145_RS09470'
            ]
            const nwk = complexTree
            const expectedNodeOrder = [ 0, 1, 2, 3, 4, 5, 8, 9, 10, 11, 12, 6, 7, 13 ]
            const tree = new Tree('debug')
            tree.buildTree(nwk)
            //console.log(tree.nodes.map(n => n.name).filter(n => n !== ''))
            const treeNode = tree.getRootNode()
            treeNode && tree.ladderize(treeNode.id)
            const nodeIds = tree.getOrder()
            const finalOrder =nodeIds.map(n => tree.getNode(n).name).filter(n => n !== '')
            //console.log(finalOrder)
            expect(nodeIds).eql(expectedNodeOrder)
            expect(finalOrder).eql(expectedFinalOrder)
        })
    })
    describe('writeNewick', () => {
      it('should write a newick tree correctly', () => {
        const nwk = '((A2lice:2,D3ebbie:3)1N:4,((B6ob:3,C7harlie:4,((E10llen:3,F11elix:4)N9:2,G12il:5)N8:3)N5:2,I13lly:5)N4:3)0N;'
        const tree = new Tree(levelLog)
        tree.buildTree(nwk)
        const newickString = tree.writeNewick()
        expect(newickString).eql(nwk)
      })
      it('should write a newick tree correctly', () => {
        const nwk = '((A2lice:2,D3ebbie:3)1N:4,((B6ob:3,C7harlie:4,((E10llen:3,F11elix:4)N9:2,G12il:5)N8:3)N5:2,I13lly:5)N4:3)0N;'
        const expectedNewick = '((A2lice:2,D3ebbie:3)1N:4,(I13lly:5,(B6ob:3,C7harlie:4,(G12il:5,(E10llen:3,F11elix:4)N9:2)N8:3)N5:2)N4:3)0N;'
        const tree = new Tree(levelLog)
        tree.buildTree(nwk).ladderize(0).ladderize(0)
        const newickString = tree.writeNewick()
        expect(newickString).eql(expectedNewick)
      })
    })
    describe.skip('selectBalancedLeafs', () => {
        it('should throw if number of mandatory picks is larger than total sample size.', () => {
            const nwk = '((A2lice:2,D3ebbie:3)1N:4,((B6ob:3,C7harlie:4,((E10llen:3,F11elix:4)N9:2,G12il:5)N8:3)N5:2,I13lly:5)N4:3)0N;'
            const tree = new Tree(levelLog)
            tree.buildTree(nwk)
            expect(() => tree.selectBalancedLeafs([2,3,7,11], 0, 3)).to.throw('The total size of the sample passed in the parameter N is smaller than the number (4) of elements passed.')
        })
        it('should throw if mandatory picks contains elements not present in tree.', () => {
            const nwk = '((A2lice:2,D3ebbie:3)1N:4,((B6ob:3,C7harlie:4,((E10llen:3,F11elix:4)N9:2,G12il:5)N8:3)N5:2,I13lly:5)N4:3)0N;'
            const tree = new Tree(levelLog)
            tree.buildTree(nwk)
            expect(() => tree.selectBalancedLeafs([2,300], 0, 5)).to.throw('Element 300 not present in tree.')
        })
        it('should work', function () {
            this.timeout(10000)
            const nwk = '((A2lice:2,D3ebbie:3)1N:4,((B6ob:3,C7harlie:4,((E10llen:3,F11elix:4)N9:2,G12il:5)N8:3)N5:2,I13lly:5)N4:3)0N;'
            const tree = new Tree(levelLog)
            tree.buildTree(nwk)
            const rounds = 100
            const rootId = 0
            const numberOfPicks = 4
            const picked: {[id:number]: number} = {
                2: 0,
                3: 0,
                6: 0,
                7: 0,
                10: 0,
                11: 0,
                12: 0,
                13: 0
            }
            for (let i = 0; i < rounds; i++) {         
                const picks = tree.selectBalancedLeafs([], rootId, numberOfPicks)
                picks.forEach((pick) => {
                    if (picked[pick]) {
                        picked[pick] += 1/rounds
                    }
                    else {
                        picked[pick] = 1/rounds
                    }
                })
            }
            const expected: {[id:number]: number} = {
                2: 0.69,
                3: 0.68,
                6: 0.57,
                7: 0.53,
                10: 0.40,
                11: 0.37,
                12: 0.38,
                13: 0.37
            }
            const tolerance = 0.02
            const leafs = tree.getAllLeafIds()
            console.log(picked)
            leafs.forEach((leaf) => {
                expect(picked[leaf]).to.exist
                expect(expected[leaf]).to.exist
                const error = Math.abs(picked[leaf] - expected[leaf])
                expect(error).to.be.lessThan(tolerance)
            })
        })
        it('should work with subtree', () => {
            const nwk = '((A2lice:2,D3ebbie:3)1N:4,((B6ob:3,C7harlie:4,((E10llen:3,F11elix:4)N9:2,G12il:5)N8:3)N5:2,I13lly:5)N4:3)0N;'
            const tree = new Tree(levelLog)
            tree.buildTree(nwk)
            const rounds = 10000
            const baseNodeId = 5
            const numberOfPicks = 4
            const picked: {[id:number]: number} = {
                2: 0,
                3: 0,
                6: 0,
                7: 0,
                10: 0,
                11: 0,
                12: 0,
                13: 0
            }
            for (let i = 0; i < rounds; i++) {         
                const picks = tree.selectBalancedLeafs([], baseNodeId, numberOfPicks)
                picks.forEach((pick) => {
                    if (picked[pick]) {
                        picked[pick] += 1/rounds
                    }
                    else {
                        picked[pick] = 1/rounds
                    }
                })
            }
            const expected: {[id:number]: number} = {
                2: 0,
                3: 0,
                6: 0.90,
                7: 0.90,
                10: 0.78,
                11: 0.75,
                12: 0.66,
                13: 0
            }
            const tolerance = 0.02
            const leafs = tree.getAllLeafIds()
            leafs.forEach((leaf) => {
                expect(picked[leaf]).to.exist
                expect(expected[leaf]).to.exist
                const error = Math.abs(picked[leaf] - expected[leaf])
                expect(error).to.be.lessThan(tolerance)
            })
        })
        it('should work with mandatory picks', function () {
            this.timeout(10000)
            const nwk = '((A2lice:2,D3ebbie:3)1N:4,((B6ob:3,C7harlie:4,((E10llen:3,F11elix:4)N9:2,G12il:5)N8:3)N5:2,I13lly:5)N4:3)0N;'
            const tree = new Tree(levelLog)
            tree.buildTree(nwk)
            const rounds = 10000
            const rootId = 0
            const numberOfPicks = 4
            const mandatoryPicks = [3, 11]
            const picked: {[id:number]: number} = {
                2: 0,
                3: 0,
                6: 0,
                7: 0,
                10: 0,
                11: 0,
                12: 0,
                13: 0
            }
            for (let i = 0; i < rounds; i++) {         
                const picks = tree.selectBalancedLeafs(mandatoryPicks, rootId, numberOfPicks)
                picks.forEach((pick) => {
                    if (picked[pick]) {
                        picked[pick] += 1/rounds
                    }
                    else {
                        picked[pick] = 1/rounds
                    }
                })
            }
            const expected: {[id:number]: number} = {
                2: 0.44,
                3: 1,
                6: 0.49,
                7: 0.38,
                10: 0.30,
                11: 1,
                12: 0.20,
                13: 0.20
            }
            const tolerance = 0.03
            const leafs = tree.getAllLeafIds()
            leafs.forEach((leaf) => {
                expect(picked[leaf]).to.exist
                expect(expected[leaf]).to.exist
                const error = Math.abs(picked[leaf] - expected[leaf])
                expect(error, `node: ${leaf}`).to.be.lessThan(tolerance)
            })
        })
    })
    describe('getListOfNodesInSubTree', () => {
        it('should return a list of TreeNodes of the subtree rooted by a given node', () => {
            const nwk = '((A2lice:2,D3ebbie:3)1N:4,((B6ob:3,C7harlie:4,((E10llen:3,F11elix:4)N9:2,G12il:5)N8:3)N5:2,I13lly:5)N4:3)0N;'
            const tree = new Tree(levelLog)
            const subNode = 5
            tree.buildTree(nwk)
            const expectedNodeIds = [5,6,7,8,9,10,11,12]
            const subTreeNodeIds = tree.getListOfNodesInSubTree(subNode).map(n => n.id)
            expect(subTreeNodeIds).eql(expectedNodeIds)
        })
    })
    describe('getSubTree', () => {
        it('should return a Tree object of the subtree rooted by a given node', () => {
            const nwk = '((A2lice:2,D3ebbie:3)1N:4,((B6ob:3,C7harlie:4,((E10llen:3,F11elix:4)N9:2,G12il:5)N8:3)N5:2,I13lly:5)N4:3)0N;'
            const tree = new Tree(levelLog)
            const subNode = 5
            tree.buildTree(nwk)
            const expectedNwk = '(B6ob:3,C7harlie:4,((E10llen:3,F11elix:4)N9:2,G12il:5)N8:3)N5:2;'
            const expectedTree = new Tree(levelLog)
            expectedTree.buildTree(expectedNwk)

            const subTree = tree.getSubTree(subNode)
            const subTreeNodes = subTree.nodes
            const expectedTreeNodes = expectedTree.nodes
            const nwkTest = subTree.writeNewick(5)
            expect(subTreeNodes).eql(expectedTreeNodes)
        })
    })
    describe('setChildrenBranchColor', () => {})
})
