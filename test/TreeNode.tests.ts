// tslint:disable: no-unused-expression
import { expect } from 'chai'
import { TreeNode } from '../src/TreeNode'

describe('TreeNode', () => {
    describe('constructor', () => {
        const name:string = 'node'
        const branchLength:number = 10
        const id: number = 0
        const node = new TreeNode(name, branchLength, id)
        it('it should have the property id', () => {
            expect(node.id).eql(id)
        })
        it('it should have the property name', () => {
            expect(node.name).eql(name)
        })
        it('children should be empty array', () => {
            expect(node.children).eql([])
        })
        it('it should have the property branchLength', () => {
            expect(node.branchLength).eql(branchLength)
        })
        it('the property parentNodeId should be null', () => {
            const parentNodeId = node.getParentNodeId()
            expect(parentNodeId).to.be.null
        })
    })
    describe('getParentNodeId & setParentNodeId', () => {
        const name:string = 'node'

        const branchLength:number = 10
        const id = 0
        const node = new TreeNode(name, branchLength, id)
        it('it should set parent', () => {
            const parentNodeId = 2
            node.setParentNodeId(parentNodeId)
            const currentParentNodeId = node.getParentNodeId()
            expect(currentParentNodeId).eql(parentNodeId)
        })
        it('it should allow to set the parent again - for reroot for example', () => {
            const newParentNodeId = 1
            node.setParentNodeId(newParentNodeId)
            const currentParentNodeId = node.getParentNodeId()
            expect(currentParentNodeId).eql(newParentNodeId)
        })
    })
    describe('isLeaf', () => {
        const name:string = 'node'
        const branchLength:number = 10
        const id = 0
        const node = new TreeNode(name, branchLength, id)
        it('it should return true if there is no children', () => {
            const isLeaf = node.isLeaf()
            expect(isLeaf).to.be.true
        })
        it('it should return false if there is at least one children', () => {
            node.children.push(1)
            const isLeaf = node.isLeaf()
            expect(isLeaf).to.be.false
        })
    })
    describe('isRoot, setRoot and unsetRoot', () => {
        const name:string = 'node'
        const branchLength:number = 10
        const id = 0
        const node = new TreeNode(name, branchLength, id)
        it('it should return false by default', () => {
            const isRoot = node.isRoot()
            expect(isRoot).to.be.false
        })
        it('it should return true if the node is set as root', () => {
            node.setAsRoot()
            const isRoot = node.isRoot()
            expect(isRoot).to.be.true
        })
        it('it should return false if the node is unset as root', () => {
            node.unsetAsRoot()
            const isRoot = node.isRoot()
            expect(isRoot).to.be.false
        })
    })
    describe('getLabelWidth & setLabelWidth', () => {
        it('should get and set the width of label of the node', () => {
            const name:string = 'node'

            const branchLength:number = 10
            const id = 0
            const node = new TreeNode(name, branchLength, id)
            const labelWidth = 4
            node.setLabelWidth(labelWidth)
            expect(node.getLabelWidth()).eql(labelWidth)
        })
    })
    describe('getLabelHeight & setLabelHeight', () => {
        it('should get and set the height of label of the node', () => {
            const name:string = 'node'

            const branchLength:number = 10
            const id = 0
            const node = new TreeNode(name, branchLength, id)
            const labelHeight = 4
            node.setLabelHeight(labelHeight)
            expect(node.getLabelHeight()).eql(labelHeight)
        })
    })
    describe('getDefaultLabelColor & setDefaultLabelColor', () => {
        it('should get and set the color of label of the node', () => {
            const name:string = 'node'

            const branchLength:number = 10
            const id = 0
            const node = new TreeNode(name, branchLength, id)
            const labelColor = 'red'
            node.setDefaultLabelColor(labelColor)
            expect(node.getDefaultLabelColor()).eql(labelColor)
        })
    })
    describe('getNodeColor & setNodeColor', () => {
        it('should get and set the color of the node', () => {
            const name:string = 'node'

            const branchLength:number = 10
            const id = 0
            const node = new TreeNode(name, branchLength, id)
            const nodeColor = '#FFFFFF'
            node.setNodeColor(nodeColor)
            expect(node.getNodeColor()).eql(nodeColor)
        })
    })
    describe('getNodeShape & setNodeShape', () => {
        it('should get and set the shape of the node', () => {
            const name:string = 'node'

            const branchLength:number = 10
            const id = 0
            const node = new TreeNode(name, branchLength, id)
            const nodeShape = 'square'
            node.setNodeShape(nodeShape)
            expect(node.getNodeShape()).eql(nodeShape)
        })
    })
    describe('getBranchPath & setBranchPath', () => {
        it('should get and set the path of the branch', () => {
            const name:string = 'node'

            const branchLength:number = 10
            const id = 0
            const node = new TreeNode(name, branchLength, id)
            const branchPath = 'M0'
            node.setBranchPath(branchPath)
            expect(node.getBranchPath()).eql(branchPath)
        })
    })
    describe('getPosX & setPosX', () => {
        it('should get and set the x position coordinate of the node', () => {
            const name:string = 'node'

            const branchLength:number = 10
            const id = 0
            const node = new TreeNode(name, branchLength, id)
            const posX = 123
            node.setPosX(posX)
            expect(node.getPosX()).eql(posX)
        })
    })
    describe('getPosY & setPosY', () => {
        it('should get and set the x position coordinate of the node', () => {
            const name:string = 'node'

            const branchLength:number = 10
            const id = 0
            const node = new TreeNode(name, branchLength, id)
            const posY = 123
            node.setPosY(posY)
            expect(node.getPosY()).eql(posY)
        })
    })
    describe('getPosR & setPosR', () => {
        it('should get and set the r position coordinate of the node', () => {
            const name:string = 'node'

            const branchLength:number = 10
            const id = 0
            const node = new TreeNode(name, branchLength, id)
            const posR = 123
            node.setPosR(posR)
            expect(node.getPosR()).eql(posR)
        })
    })
    describe('getPosA & setPosA', () => {
        it('should get and set the x position coordinate of the node', () => {
            const name:string = 'node'

            const branchLength:number = 10
            const id = 0
            const node = new TreeNode(name, branchLength, id)
            const posA = 123
            node.setPosA(posA)
            expect(node.getPosA()).eql(posA)
        })
    })
    describe('getDefaultFontSize & setDefaultFontSize', () => {
        it('should get and set the font size of the label', () => {
            const name:string = 'node'

            const branchLength:number = 10
            const id = 0
            const node = new TreeNode(name, branchLength, id)
            const fontSize = '20px'
            node.setDefaultFontSize(fontSize)
            expect(node.getDefaultFontSize()).eql(fontSize)
        })
    })
    describe('getShowLabel & setShowLabel', () => {
        it('should get and set the font size of the label', () => {
            const name:string = 'node'

            const branchLength:number = 10
            const id = 0
            const node = new TreeNode(name, branchLength, id)
            const showLabel = false
            node.setShowLabel(showLabel)
            expect(node.getShowLabel()).eql(showLabel)
        })
    })
    describe('getDisplayName & setDisplayName', () => {
        it('should get and set the diplayName', () => {
            const name:string = 'node'
            const branchLength:number = 10
            const id = 0
            const node = new TreeNode(name, branchLength, id)
            const newDisplayName = 'node 2'
            const expectedDefaultDisplayName = name
            const defaultDisplayName = node.getDisplayName()
            expect(defaultDisplayName).eql(expectedDefaultDisplayName)
            node.setDisplayName(newDisplayName)
            expect(node.getDisplayName()).eql(newDisplayName)
        })
    })
    describe('sets should be chainned', () => {
        it('should get and set the diplayName', () => {
            const name:string = 'node'
            const branchLength:number = 10
            const id = 0
            const node = new TreeNode(name, branchLength, id)
            const newDisplayName = 'node 2'
            const nodeColor = '#FFFFFF'
            node.setDisplayName(newDisplayName)
                .setNodeColor(nodeColor)
            expect(node.getDisplayName()).eql(newDisplayName)
            expect(node.getNodeColor()).eql(nodeColor)
        })
    })
})
